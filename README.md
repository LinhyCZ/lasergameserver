# LaserGameServer

Server for laser game - part of bachelor's work at UWB.

## Information

Server is build using generic Java Sockets for communication with guns implementing simple one message confirm buffer.

Communication with terminals is provided by REST API built with Spring, OpenAPI and is using API First architecture - meaning
the source code is generated from the OpenAPI specification.

Data is stored in MSSQL Database using Spring query for saving and retrieving data.

##Build
To build the server use mvn package command. 

##API specification
Specification of REST API using OpenAPIv3 specification is stored in service-config/service-api/service-api.yaml file.
Use editor.swagger.io to pretty print the yaml file. 

## TESTS
* JUnits
  * SocketMessage.java
* Integration tests
  * AuthenticationCotrollerTest.java
    * Tests correct login
    * Tests login with incorrect credentials
    * Tests logout
    * Tests logover
  * StatusControllerTest.java
    * Tests returning of data from status endpoint
    * Tests 401 Unauthorized response if not logged in
  * RegisterTest.java
    * Tests correct registration and successful login after register
    * Tests failed registration with invalid parameters
  * DesktopTest.java
    * Tests if saving lastPage to database works correctly

## TODO Terminál
* [x] Dodělat zobrazení běžící hry.
  * [x] Zobrazit položky na mapě přes Leaflet.
## TODO Zbraň
* [x] Do zbraně přidat kontrolu, že stop() nezavolá stop akci pokud isStarted == false
  *  [x] Zobrazit připraveno ke hře až po LOGINOK. Pokud LOGINERR, napiš chybu.

## TODO Celkově
* Kompletace vesty
  * [x] Vytvořit zbraň - Mám jen prototyp, potřebuju 2
    * [x] Dokončit jeden modul
  * [x] Komunikace s moduly - Už umím načíst senzory a I2C funguje. Teď připojit zbraň a displej
    * Co si budou moduly vůbec komunikovat?
      * [x] Modul -> A9G zásah ID - SHOT
      * [x] A9G -> Modul - po zásahu - DEACTIVATE
      * [x] A9G -> Modul - respawn - ACTIVATE
      * [x] A9G -> Zbraň - OUTOFAMMO
      * [x] Zbraň -> A9G - RELOAD
      * [x] A9G -> Zbraň - RELOAD COMPLETE
      * [x] Modul -> A9G - NOINFO - Když se vesta zeptá na zprávu, vrať NOINFO, ať nezdržuješ timeoutem
      * [x] Modul -> A9G - INIT - Mělo by tam být i ID hráče (Ať zbraň má co posílat) a barva týmu (Ať moduly ví, jakou zobrazit barvu)
        * [x] Default stav modulů bude DEACTIVATED
        * [x] Až začne hra, A9G dostane zprávu a aktivuje moduly
    * Socket zprávy
      * [x] LOGIN
        * Přihlásí vestu do systému - Natsaví její status na online.
      * [x] PREPARE
        * Předá jména hráčů, IDčka a týmy do vesty, aby věděla, kdo ho zasáhl
        * Odpoví PREPAREDONE - Vše je nastaveno, vesta je ready.
        * Závazný fromát: [MSGID]\_PREP\_[PlayerID]\_[TeamID]\_[Player1ID]\_[Team1ID]\_[Name1]\_[Player2ID]\_[Team2ID]\_[Name2]\_....
      * [x] SHOT - Předá ID kdo hráče zasáhl
      * [x] START
        * Předá čas startu hry. A9G si načte RTC čas ze sítě, a periodicky kontroluje,
        * zda už je čas aktivovat moduly.
        * Odpoví STARTDONE. Pokud všechny vesty nestihnou včas odpovědět START_DONE, zruší hru.
        * Závazný fromát: [MSGID]\_START\_[AMMO]\_[LIVES]\_[SHOTDAMAGE]\_[RESPAWNPERIOD]\_[EPOCHSTART]\_[EPOCHEND]
      * [x] STOP
        * Stopne vesty. Deaktivuje moduly, oznámí hráčům, ať se vrátí na start.
        * Po stopnutí A9G odešle SUMMARY - zatím asi jen počet zásahů.
      * Optional
        * [x] POS - Pošle pozici vesty na server. Může být periodicky, třeba každých 5 sekund.
        * [x] OUTOFPOS !WILL NOT DO! - Pošle server na vestu. Vesta dá 10 sekund timeout, pak se deaktivuje zbraň.
        * [x] INPOS !WILL NOT DO! - Pošle sever na vestu. Aktivuje zbraň, vymaže timeout.

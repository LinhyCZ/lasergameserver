def version = "UNITIALIZED"

def getCommandOutput(cmd) {
   stdout = bat(returnStdout:true , script: cmd).trim()
   result = stdout.readLines().drop(1).join(" ")
   return result
}

pipeline {
    agent any
    tools {
        maven 'Maven 3.8.1'
        jdk 'jdk11'
    }
    stages {
        stage('Setup') {
            steps {
                echo 'Preparing Pipeline..'
            }
        }

        stage('Checkout code') {
            steps {
                checkout scm
            }
        }

        stage('Build') {
            steps {
                bat 'mvn clean install -DskipTests'
            }
        }

        stage('Test') {
            steps {
                bat 'mvn surefire-report:report'
                bat 'mvn site -DgenerateReports=false'

                publishHTML (target : [allowMissing: false,
                 alwaysLinkToLastBuild: true,
                 keepAll: true,
                 reportDir: 'target/site',
                 reportFiles: 'surefire-report.html',
                 reportName: 'JUnit Test Report'])
            }
        }

        stage('Copy and Cleanup') {
            steps {

                script {
                    version = getCommandOutput('mvn help:evaluate -Dexpression=project.version -q -DforceStdout')
                }

                bat "if not exist \"Z:\\Builds\\LaserGameServer\\$version\" mkdir Z:\\Builds\\LaserGameServer\\$version"
                bat "copy C:\\ProgramData\\Jenkins\\.jenkins\\workspace\\ServerBuild\\target\\laserGameServer* Z:\\Builds\\LaserGameServer\\$version"
                bat "move C:\\ProgramData\\Jenkins\\.jenkins\\workspace\\ServerBuild\\target\\laserGameServer-${version}.jar Z:\\Builds\\AllLatest\\LaserGameServer.jar"
            }
        }

        stage('Deploy') {
            steps {
                script {
                    try {
                        bat "net stop LaserGameServer"
                    } catch (err) {
                        echo err.getMessage()
                    }
                }

                sleep (time:5, unit:"SECONDS")
                bat "net start LaserGameServer"
            }
        }
    }
    post {
        always {
            junit "target/surefire-reports/*.xml"
            bat "rmdir /S /Q C:\\ProgramData\\Jenkins\\.jenkins\\workspace\\ServerBuild\\target"
        }
    }
}
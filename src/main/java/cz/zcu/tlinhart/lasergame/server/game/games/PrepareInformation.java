package cz.zcu.tlinhart.lasergame.server.game.games;

import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.socketServer.ISendable;
import java.util.List;

/**
 * Data class that contains information that should be sent to vest in PREPARE message
 */
public class PrepareInformation implements ISendable {
    private final List<Player> playerList;
    private int currentPlayerID;
    private int currentTeam;

    public PrepareInformation(List<Player> playerList) {
        this.playerList = playerList;
    }

    public void setCurrentPlayer(Player player) {
        this.currentPlayerID = playerList.indexOf(player);
        this.currentTeam = player.getTeam().ordinal();
    }

    /**
     * Returns PREPARE message in format PREP_[PlayerID]_[TeamID]_[Player1ID]_[Team1ID]_[Name1]_[Player2ID]_[Team2ID]_[Name2]_....
     * @return PREPARE message
     */
    @Override
    public String getSocketSendData() {
        StringBuilder builder = new StringBuilder("PREP_");

        builder.append(currentPlayerID);
        builder.append("_");
        builder.append(currentTeam);

        for (int i = 0; i < playerList.size(); i++) {
            Player p = playerList.get(i);
            builder.append("_");
            builder.append(i);
            builder.append("_");
            builder.append(p.getTeam().ordinal());
            builder.append("_");
            builder.append(p.getPlayerName());
        }

        return builder.toString();
    }
}

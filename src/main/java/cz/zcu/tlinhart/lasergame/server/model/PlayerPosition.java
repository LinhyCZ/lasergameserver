package cz.zcu.tlinhart.lasergame.server.model;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@Table(name = "Player_Position", schema = "laser_game")
public class PlayerPosition {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Player_Position_ID_Sequence")
    @SequenceGenerator(name = "Player_Position_ID_Sequence", sequenceName = "laser_game.Player_Position_ID_Sequence", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "Player")
    private Player player;

    @Column(name = "Latitude")
    private Float latitude;

    @Column(name = "Longitude")
    private Float longitude;

    @Column(name = "Date")
    private OffsetDateTime date;
}

package cz.zcu.tlinhart.lasergame.server.rest.service;

import static cz.zcu.tlinhart.lasergame.server.authentication.JwtRequestFilter.BEARER_KEYWORD;
import cz.zcu.tlinhart.lasergame.server.authentication.JwtTokenUtil;
import cz.zcu.tlinhart.lasergame.server.authentication.JwtUserDetailsService;
import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.authentication.SessionUser;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.repository.UserRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.UserLoginBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

/**
 * Service for processing data for /authentication endpoints. More information in service-api.yaml
 */
@Service
public class AuthenticationService {

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository repository;

    /**
     * Logoffs user authenticated by given JWT token
     *
     * @param authorization JWT token
     * @param request request
     * @param response response
     */
    public void logoffUser(String authorization, HttpServletRequest request, HttpServletResponse response) {
        SessionManager.getInstance().logoutUserByJWTToken(authorization);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
    }

    /**
     * Logins user by given user information
     *
     * @param userLoginBody objects containing login information
     * @return Reponse containing JWT token and last page
     * @throws Exception when login failed for some reason
     */
    public LoginResponse logonUser(UserLoginBody userLoginBody) throws Exception {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userLoginBody.getUsername(), userLoginBody.getPassword()));

        SessionUser userDetails = (SessionUser) userDetailsService.loadUserByUsername(userLoginBody.getUsername());
        userDetails.setSessionId(SessionManager.getInstance().loginUser(userLoginBody, userDetails.getId()));
        String token = BEARER_KEYWORD + jwtTokenUtil.generateToken(userDetails);

        LoginResponse response = new LoginResponse().token(token);

        User user = repository.findByUsername(userLoginBody.getUsername());
        if (user.getLastPage() != null) {
            response.setLastPage(user.getLastPage());
        }

        return response;
    }
}

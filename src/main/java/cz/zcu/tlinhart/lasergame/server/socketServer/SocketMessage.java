package cz.zcu.tlinhart.lasergame.server.socketServer;

import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessageIDException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadExceptionType;
import java.time.OffsetDateTime;

/**
 * Class for storing sent message with its given ID. Payload has to return correct value by its toString method.
 *
 * @version 9.3.2020/qlint - created
 */
public class SocketMessage {
    private final ISendable payload;
    private final int messageId;
    private OffsetDateTime sendTime;

    /**
     * Constructor to store ID and payload.
     * @param messageId ID of the new message
     * @param payload Data stored in the message
     * @throws InvalidSocketMessageIDException When messageID is less then 0
     * @throws InvalidSocketMessagePayloadException When payload is null
     */
    public SocketMessage(int messageId, ISendable payload) throws InvalidSocketMessageIDException, InvalidSocketMessagePayloadException {
        if (messageId <= 0) {
            throw new InvalidSocketMessageIDException(messageId);
        }

        if (payload == null) {
            throw new InvalidSocketMessagePayloadException(InvalidSocketMessagePayloadExceptionType.NULL);
        }

        this.payload = payload;
        this.messageId = messageId;
    }

    /**
     * Sets time when the messsage has been sent.
     *
     * @param sendTime time when the messsage has been sent
     */
    public void setSendTime(OffsetDateTime sendTime) {
        this.sendTime = sendTime;
    }

    /**
     * Returns time when the message has been sent.
     *
     * @return time when the message has been sent.
     */
    public OffsetDateTime getSendTime() {
        return sendTime;
    }

    /**
     * Returns ID of the message.
     *
     * @return ID of the message.
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * Returns message string used to communicate with device in format [ID]__[Payload]
     * @return message string
     */
    @Override
    public String toString() {
        return messageId + "_" + payload.getSocketSendData();
    }
}

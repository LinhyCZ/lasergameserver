package cz.zcu.tlinhart.lasergame.server.rest.controller;

import cz.zcu.tlinhart.lasergame.server.rest.api.VestsApi;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestCode;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestInfo;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.rest.service.VestsService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for processing /api/vests endpoints. More information in service-api.yaml
 */
@RestController
public class VestsController implements VestsApi {

    Logger logger = LoggerFactory.getLogger(VestsController.class);

    @Autowired
    VestsService service;

    @Override
    public ResponseEntity<Void> apiVestsDeleteDelete(String authorization, VestCode vestCode) throws Exception {
        HttpStatus status = service.deleteVest(authorization, vestCode);

        return new ResponseEntity<>(status);
    }

    @Override
    public ResponseEntity<List<VestInfo>> apiVestsMyVestsGet(String authorization) throws Exception {
        List<VestInfo> vests = service.getVestsForCurrentUser(authorization);

        return new ResponseEntity<>(vests, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> apiVestsRegisterPost(String authorization, VestRegisterInfo vestInfo) throws Exception {
        try {
            service.registerVestToCurrentUser(authorization, vestInfo);
        } catch (Exception e) {
            logger.error("Cannot register vest: ", e);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

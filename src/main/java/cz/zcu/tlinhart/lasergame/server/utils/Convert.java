package cz.zcu.tlinhart.lasergame.server.utils;

import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartPlayerData;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestInfo;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to convert data from game objects to REST API objects and vice versa
 */
public class Convert {
    public static VestInfo toVestInfo(Vest vest) {
        VestInfo vestToReturn = new VestInfo();

        vestToReturn.setVestCode(vest.getVestCode());
        vestToReturn.setVestName(vest.getVestName());
        switch (vest.getStatus()) {
            case ONLINE:
                vestToReturn.setStatus(VestInfo.StatusEnum.ONLINE);
                break;
            case OFFLINE:
            default:
                vestToReturn.setStatus(VestInfo.StatusEnum.OFFLINE);
                break;
        }


        return vestToReturn;
    }

    public static List<VestInfo> toListVestInfo(List<Vest> vests) {
        List<VestInfo> vestsToReturn = new ArrayList<>();

        for (Vest vest : vests) {
            vestsToReturn.add(toVestInfo(vest));
        }

        return vestsToReturn;
    }

    public static Vest toDatabaseVest(VestRegisterInfo vestInfo) {
        return toDatabaseVest(vestInfo, null);
    }

    public static Vest toDatabaseVest(VestRegisterInfo vestInfo, Long userID) {
        Vest vestToReturn = new Vest();

        vestToReturn.setVestName(vestInfo.getVestName());
        vestToReturn.setVestCode(vestInfo.getVestCode());

        if (userID != null) {
            vestToReturn.setOwner(userID);
        }

        return vestToReturn;
    }

    public static StartGameData toStartGameData(Game newGame) {
        StartGameData gameData = new StartGameData();

        gameData.setGameID(newGame.getId());
        gameData.setPlayerData(new ArrayList<>());

        gameData.setDuration(newGame.getGameDuration());
        gameData.setPlayerLives(newGame.getPlayerLives());
        gameData.setWeaponDamage(newGame.getWeaponDamage());
        gameData.setPlayerAmmo(newGame.getPlayerAmmo());
        gameData.setRespawnPeriod(newGame.getRespawnPeriod());
        gameData.setStartTimeout(newGame.getStartTimeout());

        for (Player p : newGame.getPlayerList()) {
            gameData.addPlayerDataItem(Convert.toStartPlayerData(p));
        }

        return gameData;
    }

    private static StartPlayerData toStartPlayerData(Player p) {
        StartPlayerData playerData =  new StartPlayerData();

        playerData.setPlayerName(p.getPlayerName());
        playerData.setVestCode(p.getVest().getVestCode());
        playerData.setTeam(p.getTeam());

        return playerData;
    }

    public static GameSummary toGameSummary(Game game) {
        GameSummary summary = new GameSummary();

        summary.setGameID(game.getId());
        summary.setDateOfGame(game.getStartDate());
        summary.setGameStatus(game.getStatus());
        summary.setNumberOfPlayers(game.getPlayerList().size());

        return summary;
    }
}

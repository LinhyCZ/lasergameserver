package cz.zcu.tlinhart.lasergame.server.exceptions;

/**
 * Simple exception to be extended by other SocketExceptions
 */
public class SocketException extends Exception {
    public SocketException() {
        super();
    }

    public SocketException(String message) {
        super(message);
    }
}

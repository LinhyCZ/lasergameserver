package cz.zcu.tlinhart.lasergame.server;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Application properties. Can be set in application.properties file under app.settings.[setting]
 */
@ConfigurationProperties("app.settings")
@Setter
@Getter
public class ApplicationProperties {
    /**
     * Default session timeout in hours
     */
    private int sessionTimeoutHours = 24;

    /**
     * Password hash parameter for BCryptPasswordEncoder
     */
    private int passwordHashStrength = 10;

    /**
     * Port of the socket server
     */
    private int socketServerPort = 8888;

    /**
     * How many times can socket fail parsing the message before closing the socket.
     */
    private int socketMessageWarningThreshold = 10;

    /**
     * How many seconds to wait for readLine to read data. It is used for detecting failed connections.
     */
    private int socketReadTimeout = 10;

    /**
     * Default timeout for socket client to acknowledge receiving the message.
     */
    private int messageAcknowledgeTimeoutSeconds = 5;

    /**
     * Default period for Acknowledger to check if messages were ACKed
     */
    private int acknowledgerCheckPeriod = 1;

    /**
     * Default time limit for vests to respond to PREPARE message
     */
    private int prepareResponseTimeout = 15;

    /**
     * Default time limit for vests to respond to START message
     */
    private int startResponseTimeout = 15;
}

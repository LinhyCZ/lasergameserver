package cz.zcu.tlinhart.lasergame.server.model;

import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessageIDException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadException;
import cz.zcu.tlinhart.lasergame.server.exceptions.SocketClosedException;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.game.games.PrepareInformation;
import cz.zcu.tlinhart.lasergame.server.game.games.StartInformation;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameAction;
import cz.zcu.tlinhart.lasergame.server.rest.model.GamePlayerData;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.LoggerFactory;

/**
 * Simple class containing information about currently running game. Used in Hibernate to save data to database.
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@Table(name = "Games", schema = "laser_game")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Game_ID_Sequence")
    @SequenceGenerator(name = "Game_ID_Sequence", sequenceName = "laser_game.Game_ID_Sequence", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "Start_User", nullable = false)
    private User startUser;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    private List<Player> playerList = new LinkedList<>();

    @Column(name = "Status")
    @Enumerated(EnumType.STRING)
    private GameSummary.GameStatusEnum status;

    @Column(name = "Start_Date")
    private OffsetDateTime startDate;

    @Column(name = "Game_Duration")
    private Long gameDuration;

    @Column(name = "Player_Lives")
    private Integer playerLives;

    @Column(name = "Weapon_Damage")
    private Integer weaponDamage;

    @Column(name = "Player_Ammo")
    private Integer playerAmmo;

    @Column(name = "Respawn_Period")
    private Integer respawnPeriod;

    @Column(name = "Start_Timeout")
    private int startTimeout;

    @Transient
    private OffsetDateTime endDate;

    @Transient
    private int preparedVests = 0;

    @Transient
    private Integer startedVests = 0;

    //GameActions are not stored in the Database, they are shown only during the match.
    @Transient
    private List<GameAction> actions = new ArrayList<>();

    @Transient
    @EqualsAndHashCode.Exclude
    private final Object prepareAndStartCounterLock = new Object();


    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************

    /**
     * Default constructor
     *
     * @param startUser User that started the game
     */
    public Game(User startUser) {
        this.startUser = startUser;

        this.status = GameSummary.GameStatusEnum.CREATED;
    }

    /**
     * Sets status of the game to RUNNING. Starts timer that stops the game after specified duration.
     */
    public void start() {
        this.status = GameSummary.GameStatusEnum.RUNNING;
        this.actions.add(GameActionFactory.startOfGameAction(this.actions.size()));

        long stopTime = this.endDate.minusSeconds(OffsetDateTime.now().toEpochSecond()).toEpochSecond();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                //If game was canceled by API call don't do anything
                if (Game.this.getStatus() == GameSummary.GameStatusEnum.RUNNING) {
                    try {
                        GamesManager.getInstance().stopGame(Game.this);
                    } catch (InvalidSocketMessageIDException | SocketClosedException | InvalidSocketMessagePayloadException e) {
                        //It shouldn't be necessary to send socket messages to vests, because they have timer integrated.
                        LoggerFactory.getLogger(Game.class).error("Couldn't stop game after the end.");
                    }
                }
            }
        }, stopTime * 1000);
    }

    /**
     * Sets status of the game to PREPARING.
     */
    public void startPrepare() {
        this.status = GameSummary.GameStatusEnum.PREPARING;
    }

    /**
     * Sets status of the game to STARTING. Also sets the the StartDate and EndDate.
     */
    public void prepareDone() {
        this.status = GameSummary.GameStatusEnum.STARTING;
        this.startDate = OffsetDateTime.now().plusSeconds(this.startTimeout);
        this.endDate = this.startDate.plusSeconds(this.gameDuration);
    }

    /**
     * Sets back status of the game to CREATED and zeroes the number of prepared and started vests.
     */
    public void prepareFailed() {
        this.status = GameSummary.GameStatusEnum.CREATED;

        this.preparedVests = 0;
        this.startedVests = 0;
    }

    /**
     * Sets status of the game to COMPLETED.
     */
    public void stop() {
        this.actions.add(GameActionFactory.endOfGameAction(this.actions.size()));

        this.status = GameSummary.GameStatusEnum.COMPLETED;
    }

    /**
     * Creates clone of this game containing the same list of players.
     *
     * @return Clone of this instance of game.
     */
    public Game cloneGame() {
        Game game = new Game();

        game.status = GameSummary.GameStatusEnum.CREATED;
        game.startUser = this.startUser;
        game.gameDuration = this.gameDuration;
        game.playerLives = this.playerLives;
        game.weaponDamage = this.weaponDamage;
        game.playerAmmo = this.playerAmmo;
        game.respawnPeriod = this.respawnPeriod;
        game.startTimeout = this.startTimeout;

        for (Player p : this.playerList) {
            Player clonedPlayer = p.clonePlayer();

            clonedPlayer.setGame(game);
            game.playerList.add(clonedPlayer);
        }

        return game;
    }

    /**
     * Returns PrepareInformation object with information about players that are sent in the PREPARE message.
     *
     * @return PrepareInformation object
     */
    public PrepareInformation getGenericPrepareInformation() {
        return new PrepareInformation(playerList);
    }

    /**
     * Returns StartInformation object with information about parameters of the game that are sent in the START message.
     *
     * @return StartInformation object
     */
    public StartInformation getStartInformation() {
        long startDateMillis = startDate.toEpochSecond();
        long endDateMillis = startDateMillis + gameDuration;

        return new StartInformation(weaponDamage, playerLives, playerAmmo, respawnPeriod, startDateMillis, endDateMillis);
    }

    /**
     * Adds one to the count of preparedVests.
     *
     * @return true if all vests have responded to PREPARE message, false otherwise
     */
    public boolean addPreparedVest() {
        synchronized (prepareAndStartCounterLock) {
            preparedVests++;

            return preparedVests == playerList.size();
        }
    }

    /**
     * Adds one to the count of startedVests.
     *
     * @return true if all vests have responded to START message, false otherwise
     */
    public boolean addStartedVest() {
        synchronized (prepareAndStartCounterLock) {
            startedVests++;

            return startedVests == playerList.size();
        }
    }

    /**
     * Generates list of REST API GamePlayerData objects containing information about players.
     *
     * @return filled list of REST API GamePlayerData objects
     */
    public List<GamePlayerData> generateGamePlayerDataList() {
        List<GamePlayerData> list = new ArrayList<>();

        for (Player p : playerList) {
            list.add(p.generateGamePlayerData());
        }

        return list;
    }
}

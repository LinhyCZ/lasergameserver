package cz.zcu.tlinhart.lasergame.server.exceptions;

public class InvalidSocketMessageIDException extends SocketException {
    public InvalidSocketMessageIDException(int value) {
        super("Specified ID value of " + value + " is not a valid message ID.");
    }
}

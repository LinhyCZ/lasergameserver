package cz.zcu.tlinhart.lasergame.server.game.games;

import cz.zcu.tlinhart.lasergame.server.socketServer.ISendable;
import lombok.AllArgsConstructor;

/**
 * Data class that contains information that should be sent to vest in START message
 */
@AllArgsConstructor
public class StartInformation implements ISendable {
    private int weaponDamage;
    private int playerLives;
    private int playerAmmo;
    private int respawnPeriod;
    private long startDate;
    private long endDate;

    /**
     * Returns START message in format START_[AMMO]_[LIVES]_[SHOTDAMAGE]_[RESPAWNPERIOD]_[EPOCHSTART]_[EPOCHEND]
     * @return START message
     */
    @Override
    public String getSocketSendData() {
        StringBuilder builder = new StringBuilder("START_");

        builder.append(playerAmmo);
        builder.append("_");
        builder.append(playerLives);
        builder.append("_");
        builder.append(weaponDamage);
        builder.append("_");
        builder.append(respawnPeriod);
        builder.append("_");
        builder.append(startDate);
        builder.append("_");
        builder.append(endDate);


        return builder.toString();
    }
}

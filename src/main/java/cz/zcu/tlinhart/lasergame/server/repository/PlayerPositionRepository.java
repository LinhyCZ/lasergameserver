package cz.zcu.tlinhart.lasergame.server.repository;

import cz.zcu.tlinhart.lasergame.server.model.PlayerPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Hibernate repository to access player position data in database
 */
@Repository
public interface PlayerPositionRepository extends JpaRepository<PlayerPosition, Long> {
}

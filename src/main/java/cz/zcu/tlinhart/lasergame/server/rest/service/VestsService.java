package cz.zcu.tlinhart.lasergame.server.rest.service;

import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.repository.VestRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestCode;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestInfo;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.utils.Convert;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Service for processing /api/vests endpoints. More information in service-api.yaml
 */
@Service
public class VestsService {

    @Autowired
    private VestRepository repository;

    public List<VestInfo> getVestsForCurrentUser(String JWTtoken) {
        Long userID = SessionManager.getInstance().getUserByJWTToken(JWTtoken).getId();

        List<Vest> vests = repository.findByUserid(userID);

        return Convert.toListVestInfo(vests);
    }

    public void registerVestToCurrentUser(String JWTtoken, VestRegisterInfo vestInfo) {
        Long userID = SessionManager.getInstance().getUserByJWTToken(JWTtoken).getId();

        Vest vest = Convert.toDatabaseVest(vestInfo, userID);

        VestManager.getInstance().addVest(vest);
    }

    public HttpStatus deleteVest(String authorization, VestCode vestCode) {
        Vest vest = repository.findByCode(vestCode.getVestCode());
        User user = SessionManager.getInstance().getUserByJWTToken(authorization);

        // Vest doesn't exist. We return 200 because we don't want to let
        if (vest == null) {
            return HttpStatus.NO_CONTENT;
        }

        if (!vest.getOwner().equals(user.getId())) {
            return HttpStatus.CONFLICT;
        }

        VestManager.getInstance().deleteVest(vest);

        return HttpStatus.OK;
    }
}

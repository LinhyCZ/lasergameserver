package cz.zcu.tlinhart.lasergame.server.rest.controller;

import cz.zcu.tlinhart.lasergame.server.rest.api.StatusApi;
import cz.zcu.tlinhart.lasergame.server.rest.model.StatusBody;
import cz.zcu.tlinhart.lasergame.server.rest.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for processing /api/status endpoints. More information in service-api.yaml
 */
@RestController
public class StatusController implements StatusApi {
    @Autowired
    StatusService service;

    @Override
    public ResponseEntity<StatusBody> apiStatusGet(String authorization) throws Exception {
        HttpStatus status = HttpStatus.OK;

        StatusBody applicationStatus = service.getApplicationStatus();

        return new ResponseEntity<>(applicationStatus, status);
    }
}

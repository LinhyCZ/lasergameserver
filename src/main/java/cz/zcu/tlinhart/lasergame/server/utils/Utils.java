package cz.zcu.tlinhart.lasergame.server.utils;

import javax.servlet.http.HttpServletResponse;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.http.MediaType;

/**
 * Class containing some useful Util functions :)
 */
public class Utils {
    /**
     * Checks if specified string is blank. Does the "<code>null</code>" check
     * and checks zero length of the string (without leading and trailing whitespace)
     * @param someString string to be checked
     * @return <code>true</code> if specified parameter is blank, otherwise <code>false</code>
     */
    public static boolean isBlank(String someString) {
        if (someString == null) {
            return true;
        }

        return someString.trim().length() < 1;
    }

    /**
     * Inverse value of isBlank function
     *
     * @param someString string to be checked
     * @return <code>false</code> if specified parameter is blank, otherwise <code>true</code>
     */
    public static boolean isNotBlank(String someString) {
        return !isBlank(someString);
    }

    public static <T> boolean nullableEquals(JsonNullable<T> equals, T equalsTo) {
        if (equals == null || equalsTo == null) {
            return false;
        }

        if (!equals.isPresent()) {
            return false;
        }

        return equals.get().equals(equalsTo);
    }

    public static <T> T getNullableValue(JsonNullable<T> value) {
        if (value == null) {
            return null;
        }

        if (!value.isPresent()) {
            return null;
        }

        return value.get();
    }

    public static void resetAndPrepareResponse(HttpServletResponse response) {
        response.reset();
        response.setContentType(MediaType.APPLICATION_JSON.toString());
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "*");
    }
}

package cz.zcu.tlinhart.lasergame.server.repository;

import cz.zcu.tlinhart.lasergame.server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Hibernate repository to access user data in database
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("FROM User WHERE Name = :name")
    User findByUsername(@Param("name") String username);

    @Query("SELECT CASE WHEN COUNT(user) > 0 THEN TRUE ELSE FALSE END FROM User user WHERE LOWER(user.username) LIKE LOWER(:name)")
    boolean existsByUsername(@Param("name") String username);
}

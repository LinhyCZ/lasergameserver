package cz.zcu.tlinhart.lasergame.server.authentication;

import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.repository.UserRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Spring authentication service to load users from database
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************

    /**
     * Returns details from database for given username
     *
     * @param username username
     * @return user details
     * @throws UsernameNotFoundException when user is not found
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username);

        if (user != null) {
            return new SessionUser(user.getUsername(), user.getPassword(), user.getId(), new ArrayList<>());
        }

        throw new UsernameNotFoundException("User not found with username: " + username);
    }
}

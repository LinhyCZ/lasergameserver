package cz.zcu.tlinhart.lasergame.server.socketServer;

import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessageIDException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadException;
import cz.zcu.tlinhart.lasergame.server.exceptions.SocketClosedException;
import cz.zcu.tlinhart.lasergame.server.exceptions.SocketException;
import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.utils.Utils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.time.OffsetDateTime;
import java.util.LinkedList;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread of one Socket client
 */
public class SocketThread extends Thread {
    Logger logger = LoggerFactory.getLogger(SocketThread.class);

    private final ApplicationProperties properties;

    private final Socket socket;
    private int messageCounter = 0;

    private int messageWarnings = 0;

    private SocketMessage sentBuffer;
    private final Queue<SocketMessage> messageBuffer = new LinkedList<>();

    private BufferedReader socketInput;
    private PrintWriter socketOutput;

    public SocketThread(Socket socket, ApplicationProperties properties) {
        this.socket = socket;
        this.properties = properties;
    }

    public boolean isClosed() {
        return socket.isClosed();
    }

    @Override
    public void run() {
        try {
            InputStream input = socket.getInputStream();
            socketInput = new BufferedReader(new InputStreamReader(input));

            OutputStream output = socket.getOutputStream();
            socketOutput = new PrintWriter(output, true);

            this.socket.setSoTimeout(properties.getSocketReadTimeout() * 1000);

            //Send welcome message, expect ACK
            send(() -> "Welcome to lasergame");

            String text;

            do {
                try {
                    text = socketInput.readLine();

                    if (Utils.isNotBlank(text)) {
                        if (!handleAction(text)) {
                            messageWarnings++;
                        }

                        if (messageWarnings >= properties.getSocketMessageWarningThreshold()) {
                            socket.close();
                        }
                    }

                    if (text == null) {
                        socket.close();
                    }
                } catch (java.net.SocketException e1) {
                    this.socket.close();
                } catch (SocketTimeoutException e) {
                    //Try sending ping to detect connection loss.
                    try {
                        socketOutput.println("PING");
                    } catch (Exception exception) {
                        this.socket.close();
                    }
                }
            } while (!socket.isClosed());

            socket.close();

            SocketServer.getInstance().stoppedClientThread(this);
            VestManager.getInstance().disconnectVest(VestManager.getInstance().getVestBySocket(this));

            logger.debug("Socket closed.");
        } catch (IOException | SocketClosedException | InvalidSocketMessagePayloadException | InvalidSocketMessageIDException e) {
            e.printStackTrace();
        }
    }

    private boolean handleAction(String message) throws IOException {
        if (message.equalsIgnoreCase("bye")) {
            socket.close();
            return true;
        }

        String[] messageSplit = message.split("_");

        if(messageSplit.length < 1) {
            logger.error("Provided message doesn't have enough of parameters to be parsed.");
            return false;
        }

        SocketMessageAction action = SocketMessageAction.byValue(messageSplit[0]);
        if (action == SocketMessageAction.ACK) {
            Integer messageId;
            try {
                messageId = Integer.parseInt(messageSplit[1]);
            } catch (Exception e) {
                logger.error("Couldn't parse ID from incoming message {}", message);
                return false;
            }

            if (sentBuffer != null && messageId.equals(sentBuffer.getMessageId())) {
                sentBuffer = null;

                if (messageBuffer.size() > 0) {
                    this.send(messageBuffer.remove());
                }

                return true;
            }

            /* ACK ID is different from the message we have in sentBuffer, therefore resend the message in sentBuffer */
            if (sentBuffer != null) {
                this.send(sentBuffer);
                return true;
            }
        }

        GamesManager.getInstance().processVestCommand(this, messageSplit);

        return true;
    }

    public void send(ISendable object) throws SocketClosedException, InvalidSocketMessagePayloadException, InvalidSocketMessageIDException {
        if (socket.isClosed() || !socket.isConnected()) {
            throw new SocketClosedException();
        }

        SocketMessage message;

        try {
            message = new SocketMessage(++messageCounter, object);
        } catch (InvalidSocketMessageIDException e) {
            logger.warn("Invalid socket message ID. Resetting counter and sending again.");
            messageCounter = 0;

            try {
                message = new SocketMessage(++messageCounter, object);
            } catch (InvalidSocketMessageIDException ex) {
                logger.error("FATAL: Didn't manage to send message even after counter reset. X_X");
                throw ex;
            }
        }

        if (sentBuffer == null) {
            send(message);
            return;
        }

        messageBuffer.add(message);
    }

    private void send(SocketMessage message) {
        message.setSendTime(OffsetDateTime.now());
        sentBuffer = message;
        socketOutput.println(message);
        logger.debug("Sending socket message {} to vest {}", message.toString(), VestManager.getInstance().getVestBySocket(this));
    }

    public SocketMessage getMessageInSentBuffer() {
        return sentBuffer;
    }

    public void resendMessageInSentBuffer() {
        send(sentBuffer);
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.error("Couldn't close Socket", e);
        }
    }
}

package cz.zcu.tlinhart.lasergame.server.rest.service;

import cz.zcu.tlinhart.lasergame.server.authentication.JwtTokenUtil;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.repository.UserRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.SetCurrentPageBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for processing /api/desktop endpoints. More information in service-api.yaml
 */
@Service
public class DesktopService {

    @Autowired
    JwtTokenUtil tokenUtil;

    @Autowired
    UserRepository repository;

    /**
     * Save users last page to database
     *
     * @param authorization JWT token
     * @param setCurrentPageBody object containing last page.
     */
    public void saveCurrentPage(String authorization, SetCurrentPageBody setCurrentPageBody) {
        String username = tokenUtil.getUsernameFromToken(authorization);

        User user = repository.findByUsername(username);
        user.setLastPage(setCurrentPageBody.getCurrentPage());

        repository.saveAndFlush(user);
    }
}

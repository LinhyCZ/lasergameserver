package cz.zcu.tlinhart.lasergame.server.authentication;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * DTO for storing data about user and SESSIONID. Extends default Spring user class.
 */
public class SessionUser extends User {
    public String sessionId;
    private final Long id;

    public SessionUser(String username, String password, Long id, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getId() {
        return id;
    }
}

package cz.zcu.tlinhart.lasergame.server.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO for storing information about user in database.
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@Table(name = "Users", schema = "laser_game")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserID_Sequence")
    @SequenceGenerator(name = "UserID_Sequence", sequenceName = "laser_game.UserID_Sequence", allocationSize = 1)
    private Long id;

    @Column(name = "Name")
    private String username;

    @Column(name = "Password")
    @EqualsAndHashCode.Exclude
    private String password;

    @Column(name = "Lastpage")
    @EqualsAndHashCode.Exclude
    private String lastPage;
}

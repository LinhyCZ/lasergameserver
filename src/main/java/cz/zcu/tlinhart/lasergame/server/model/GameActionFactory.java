package cz.zcu.tlinhart.lasergame.server.model;

import cz.zcu.tlinhart.lasergame.server.rest.model.GameAction;
import java.time.OffsetDateTime;

public class GameActionFactory {

    //********************************************************
    //                      CREATE METHODS
    //********************************************************
    public static GameAction startOfGameAction(int actionID) {
        return new GameAction().actionData("Hra právě začala!").actionID(actionID).actionDate(OffsetDateTime.now());
    }

    public static GameAction endOfGameAction(int actionID) {
        return new GameAction().actionData("Hra právě skončila!").actionID(actionID).actionDate(OffsetDateTime.now());
    }

    public static GameAction shotAction(Player shooter, Player hitUser, int actionID) {
        return new GameAction().actionData("Hráč " + shooter.getPlayerName() + " právě zasáhl hráče " + hitUser.getPlayerName() + "!").actionID(actionID).actionDate(OffsetDateTime.now());
    }
}

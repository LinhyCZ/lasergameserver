package cz.zcu.tlinhart.lasergame.server.socketServer;

/**
 * Socket action that can be received from A9G
 */
public enum SocketMessageAction {
    ACK, POS, LOGIN, PREPAREDONE, STARTDONE, SHOT, UNKNOWN;

    /**
     * Find enum by String value of the command
     * @param value String value of the command
     * @return instance of action
     */
    public static SocketMessageAction byValue(String value) {
        for (SocketMessageAction action : SocketMessageAction.values()) {
            if (action.name().equalsIgnoreCase(value)) {
                return action;
            }
        }

        return UNKNOWN;
    }
}

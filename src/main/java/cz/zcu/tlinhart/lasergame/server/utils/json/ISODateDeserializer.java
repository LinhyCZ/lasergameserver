package cz.zcu.tlinhart.lasergame.server.utils.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import cz.zcu.tlinhart.lasergame.server.utils.Utils;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Serializer to ISO Date format.
 * 
 * @author hors
 */
public class ISODateDeserializer extends StdDeserializer<OffsetDateTime> {
	public static final String ISO_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

	private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ISO_DATETIME_FORMAT);

	/**
	 * Basic constructor
	 */
    public ISODateDeserializer() {
        this(null);
    }

	@Override
	public OffsetDateTime deserialize(JsonParser pJsonParser,
								DeserializationContext pDeserializationContext) throws IOException, JsonProcessingException {
    	String value = pJsonParser.getText();
    	return Utils.isNotBlank(value) ? OffsetDateTime.parse(value, dateFormatter) : null;
	}

	/**
     * Basic constructor with class parameter
     *
     * @param pDateClass - class parameter
     */
    public ISODateDeserializer(Class<OffsetDateTime> pDateClass) {
        super(pDateClass);
    }
}

package cz.zcu.tlinhart.lasergame.server.exceptions;

public class InvalidSocketMessagePayloadException extends SocketException {
    private final String message;
    public InvalidSocketMessagePayloadException(InvalidSocketMessagePayloadExceptionType type) {
        switch (type) {
            case NULL:
                message = "Trying to create socket message with null payload.";
                break;
            case INVALID_CHAR:
                message = "Trying to create message with invalid characters";
                break;
            default:
                message = "Generic socket payload message exception";
                break;
        }
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getLocalizedMessage() {
        return this.message;
    }
}


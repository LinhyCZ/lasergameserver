package cz.zcu.tlinhart.lasergame.server.rest.service;

import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessageIDException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadException;
import cz.zcu.tlinhart.lasergame.server.exceptions.SocketClosedException;
import cz.zcu.tlinhart.lasergame.server.exceptions.SocketException;
import cz.zcu.tlinhart.lasergame.server.exceptions.StartGameException;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.repository.GameRepository;
import cz.zcu.tlinhart.lasergame.server.repository.PlayerPositionRepository;
import cz.zcu.tlinhart.lasergame.server.repository.PlayerRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameAction;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameDetailedSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameID;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameInformationRequestBody;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameStartException;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameRequestBody;
import cz.zcu.tlinhart.lasergame.server.utils.Convert;
import cz.zcu.tlinhart.lasergame.server.utils.Utils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Service for processing /api/games endpoints. More information in service-api.yaml
 */
@Service
public class GamesService {

    @Autowired
    GameRepository repository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    PlayerPositionRepository playerPositionRepository;

    /**
     * Creates new game
     *
     * @param authorization JWT token
     * @param startGameRequestBody ID of game to clone. May be null, empty game is then returned. If fetchData is set to true, than game data of game specified in ID is returned.
     * @return Information to show on create screen.
     */
    public StartGameData createGame(String authorization, StartGameRequestBody startGameRequestBody) {
        User user = SessionManager.getInstance().getUserByJWTToken(authorization);
        Game newGame = null;

        if (startGameRequestBody != null && Utils.nullableEquals(startGameRequestBody.getFetchData(), true)) {
            Long gameID = Utils.getNullableValue(startGameRequestBody.getGameID());

            if (gameID != null) {
                Game game = GamesManager.getInstance().getGameByIdForUser(gameID, user);

                if (game != null
                        && GameSummary.GameStatusEnum.CREATED.equals(game.getStatus())) {
                    return Convert.toStartGameData(game);
                }
            }

            return null;
        }

        if (GamesManager.getInstance().hasRunningGames(user)) {
            return null;
        }

        Long cloneGameId = (startGameRequestBody != null ? Utils.getNullableValue(startGameRequestBody.getGameID()) : null);
        if (cloneGameId != null) {
            Game gameToClone = repository.findById(cloneGameId, user);

            if (gameToClone != null) {
                newGame = gameToClone.cloneGame();
            }
        }

        if (newGame == null) {
            newGame = new Game(user);
            newGame.setPlayerList(new LinkedList<>());
        }

        repository.saveAndFlush(newGame);
        GamesManager.getInstance().createGame(newGame);

        return Convert.toStartGameData(newGame);
    }

    public HttpStatus deleteGame(String authorization, GameID gameID) {
        User user = SessionManager.getInstance().getUserByJWTToken(authorization);
        Game game = repository.findById(gameID.getGameID(), user);

        if (game == null) {
            return HttpStatus.CONFLICT;
        }

        if (game.getStatus() == GameSummary.GameStatusEnum.PREPARING || game.getStatus() == GameSummary.GameStatusEnum.RUNNING || game.getStatus() == GameSummary.GameStatusEnum.STARTING) {
            return HttpStatus.NOT_ACCEPTABLE;
        }

        GamesManager.getInstance().getRunningGames().remove(game);

        for (Player p : game.getPlayerList()) {
            playerPositionRepository.deleteAll(p.getPlayerPositions());
            playerPositionRepository.flush();
        }

        playerRepository.deleteAll(game.getPlayerList());
        playerRepository.flush();

        repository.delete(game);
        repository.flush();

        return HttpStatus.OK;
    }

    public List<GameSummary> getMyGames(String authorization) {
        List<GameSummary> gameSummaries = new ArrayList<>();
        User user = SessionManager.getInstance().getUserByJWTToken(authorization);

        for (Game game : repository.findByUser(user)) {
            gameSummaries.add(Convert.toGameSummary(game));
        }

        return gameSummaries;
    }

    public GameStartException startGame(String authorization, StartGameData startGameData) throws Exception {
        User currentUser = SessionManager.getInstance().getUserByJWTToken(authorization);

        try {
            GamesManager.getInstance().prepareAndStartGame(currentUser, startGameData);
        } catch (Exception e) {
            GameStartException exception = new GameStartException();

            if (e instanceof StartGameException) {
                StartGameException gameException = (StartGameException) e;
                exception.code(GameStartException.CodeEnum.GAME_START_EXCEPTION);
                exception.detailedInformation(gameException.getMessage());
            } else if (e instanceof SocketException) {
                exception.code(GameStartException.CodeEnum.SOCKET_EXCEPTION);
                exception.detailedInformation(e.getMessage());
            } else {
                exception.code(GameStartException.CodeEnum.INTERNAL_SERVER_ERROR);
                exception.detailedInformation(e.getMessage());
            }

            return exception;
        }

        return null;
    }

    public void stopGameForUser(String authorization) throws InvalidSocketMessageIDException, SocketClosedException, InvalidSocketMessagePayloadException {
        User user = SessionManager.getInstance().getUserByJWTToken(authorization);

        Game game = GamesManager.getInstance().getRunningGameForUser(user);

        if (game != null) {
            GamesManager.getInstance().stopGame(game);
        }
    }

    public GameDetailedSummary getDetailedSummary(String authorization, GameInformationRequestBody gameInformationRequestBody) {
        User user = SessionManager.getInstance().getUserByJWTToken(authorization);

        //Get running game
        Game game = GamesManager.getInstance().getRunningGameForUser(user);

        //Try getting recently completed game. This game still should contain information about actions.
        if (game == null) {
            game = GamesManager.getInstance().getCompletedGameForUser(user);
        }

        //If user specified game ID and it doesn't match the found game, delete it and load it from DB
        if (game != null
                && gameInformationRequestBody != null
                && gameInformationRequestBody.getGameID() != null
                && !game.getId().equals(gameInformationRequestBody.getGameID())) {
            game = null;
        }

        //Try getting game from database.
        if (game == null && gameInformationRequestBody.getGameID() != null) {
            game = GamesManager.getInstance().getGameByIdForUser(gameInformationRequestBody.getGameID(), user);
        }

        //Couldn't find anything anywhere, give up.
        if (game == null) {
            return null;
        }

        if (!GameSummary.GameStatusEnum.RUNNING.equals(game.getStatus()) && !GameSummary.GameStatusEnum.COMPLETED.equals(game.getStatus())) {
            return null;
        }

        GameDetailedSummary summary = new GameDetailedSummary();
        summary.setGameStatus(GameDetailedSummary.GameStatusEnum.fromValue(game.getStatus().getValue())); //Use this weird structure, because OpenAPI generator creates two separate Enums. Doh.

        if (game.getEndDate() == null) {
            summary.setGameEndTime(game.getStartDate().plusSeconds(game.getGameDuration()));
        } else {
            summary.setGameEndTime(game.getEndDate());
        }

        int lastActionIDValue = 0;
        if (gameInformationRequestBody != null && gameInformationRequestBody.getLastActionID() != null) {
            lastActionIDValue = gameInformationRequestBody.getLastActionID();
        }

        if (lastActionIDValue <= game.getActions().size()) {
            List<GameAction> newActions = game.getActions().subList(lastActionIDValue, game.getActions().size());
            summary.setActionData(newActions);
        } else {
            summary.setActionData(new ArrayList<>());
        }

        summary.setPlayerData(game.generateGamePlayerDataList());

        return summary;
    }
}

package cz.zcu.tlinhart.lasergame.server.game;

import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.repository.VestRepository;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketThread;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Manager for all Vests. Handles connection and disconnection and finds vest by various parameters
 */
@Component
@Scope(value = "singleton")
public class VestManager {
    private final Map<String, Vest> connectedVests = new HashMap<>();
    private List<Vest> allVests = new ArrayList<>();
    private static VestManager instance;

    @Autowired
    VestRepository repository;

    private VestManager() {
        // Singleton constructor
    }

    @PostConstruct
    private VestManager initializeInstance() {
        instance = this;
        allVests = repository.findAll();
        return instance;
    }

    public static VestManager getInstance() {
        return instance;
    }

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************

    /**
     * Adds vest to connectedVests list.
     *
     * @param vest vest to connect
     */
    public void connectVest(Vest vest) {
        synchronized (connectedVests) {
            connectedVests.put(vest.getVestCode(), vest);
        }
    }

    /**
     * Removes vest from connectedVests list
     *
     * @param vest vest to disconnect
     */
    public void disconnectVest(Vest vest) {
        if (vest != null) {
            disconnectVestByCode(vest.getVestCode());
        }
    }

    /**
     * Returns if vest is online or offline
     *
     * @param vest vest to check
     * @return true if connected or false if disconnected.
     */
    public VestStatus getVestStatus(Vest vest) {
        if(connectedVests.containsKey(vest.getVestCode())) {
            return VestStatus.ONLINE;
        }

        return VestStatus.OFFLINE;
    }

    /**
     * Sets Vest with given code to ONLINE status
     * USED PURELY FOR DEBUGGING!
     *
     * @param code Vest code
     */
    public void connectVestByCode(String code) {
        Vest vest = getVestByCode(code);

        connectVest(vest);
    }

    /**
     * Disconnect vest with given code from server.
     *
     * @param code VestCode
     */
    public void disconnectVestByCode(String code) {
        Vest vest = connectedVests.get(code);

        if (vest != null) {
            SocketThread socket = vest.getSocket();
            if (socket != null) {
                socket.close();
            }

            connectedVests.remove(code);
        }
    }

    /**
     * Find vest by vest code
     *
     * @param vestCode vest code
     * @return vest instance. May be null if not found
     */
    public Vest getVestByCode(String vestCode) {
        for (Vest vest : allVests) {
            if (vest.getVestCode().equals(vestCode)) {
                return vest;
            }
        }

        return null;
    }

    /**
     * Save vest to repository. Adds also to internal cache.
     *
     * @param vest vest to save
     */
    public void addVest(Vest vest) {
        allVests.add(vest);

        repository.saveAndFlush(vest);
    }

    /**
     * Delete vest from repository. Removes also from internal cache.
     *
     * @param vest Instance of vest to delete
     */
    public void deleteVest(Vest vest) {
        allVests.remove(vest);

        repository.delete(vest);
        repository.flush();
    }

    /**
     * Find vest instance by socket isntance.
     *
     * @param socket socket instance
     * @return vest instance. May return null if vest not found
     */
    public Vest getVestBySocket(SocketThread socket) {
        List<Vest> vests = connectedVests.values().stream().filter((vest) -> socket.equals(vest.getSocket())).collect(Collectors.toList());

        if (vests.size() == 0) {
            return null;
        }

        return vests.get(0);
    }

    /**
     * Remove all vests from repostiory and internal cache.
     */
    public void deleteAll() {
        allVests.clear();
        connectedVests.clear();

        repository.deleteAll();
        repository.flush();
    }
}

package cz.zcu.tlinhart.lasergame.server.socketServer;

import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class of Socket Server.
 */
public class SocketServer extends Thread {
    private static SocketServer instance;

    private final Object socketThreadsLock = new Object();

    private final List<SocketThread> threads = new ArrayList<>();
    private int socketServerPort;
    private ApplicationProperties properties;

    private boolean isStarted = false;

    Logger logger = LoggerFactory.getLogger(SocketServer.class);
    
    private SocketServer() {
        // Empty constructor.
    }

    public static SocketServer getInstance() {
        if (instance == null) {
            instance = new SocketServer();
        }
        
        return instance;
    }

    /**
     * Start new instance of Socket server
     * @param properties Application properties
     */
    public static void startServer(ApplicationProperties properties) {
        SocketServer server = SocketServer.getInstance();

        if (!server.isStarted) {
            server.initialize(properties).start();
        }
    }

    /**
     * Main worker thread of the Socket server. Dispatches connections to their worker threads.
     */
    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(socketServerPort)) {

            logger.info("Server is listening on port {}", socketServerPort);

            while (isStarted) {
                Socket socket = serverSocket.accept();
                logger.debug("New client connected");

                SocketThread thread = new SocketThread(socket, properties);

                synchronized (socketThreadsLock) {
                    threads.add(thread);
                }

                thread.start();
            }

        } catch (IOException ex) {
            logger.error("Server exception: ", ex);
            ex.printStackTrace();
        }
    }

    /**
     * Initialize new instance of SocketServer. Starts the server and Acknowledger
     *
     * @param properties Application properties
     * @return SocketServer singleton instance
     */
    public SocketServer initialize(ApplicationProperties properties) {
        this.properties = properties;
        this.socketServerPort = properties.getSocketServerPort();
        this.isStarted = true;

        Acknowledger.initialize(properties).start();

        return instance;
    }

    /**
     * Stops the SocketServer and Acknowledger
     */
    public void stopServer() {
        Acknowledger.getInstance().stopAcknowledger();
        isStarted = false;
    }

    /**
     * Remove client thread from the list of open threads.
     *
     * @param thread to be removed
     */
    protected void stoppedClientThread(SocketThread thread) {
        synchronized (socketThreadsLock) {
            this.threads.remove(thread);
        }
    }

    /**
     * Returns lock used for synchronizing thread list.
     *
     * @return socket lock.
     */
    public Object getSocketThreadsLock() {
        return socketThreadsLock;
    }

    /**
     * Returns list of client threads.
     * @return List of client threads connected to the server.
     */
    public List<SocketThread> getClientThreads() {
        return this.threads;
    }
}

package cz.zcu.tlinhart.lasergame.server.repository;

import cz.zcu.tlinhart.lasergame.server.model.Vest;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Hibernate repository to access vest data in database
 */
public interface VestRepository extends JpaRepository<Vest, Long> {
    @Query("FROM Vest WHERE vestCode = :code")
    Vest findByCode(@Param("code") String code);

    @Query("FROM Vest WHERE owner = :owner")
    List<Vest> findByUserid(@Param("owner") Long userID);
}

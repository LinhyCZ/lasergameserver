package cz.zcu.tlinhart.lasergame.server.model;

/**
 * Enum to determine vest status
 */
public enum VestStatus {
    ONLINE, OFFLINE
}

package cz.zcu.tlinhart.lasergame.server.authentication;

import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import cz.zcu.tlinhart.lasergame.server.exceptions.NotLogoverException;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.repository.UserRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.UserLoginBody;
import cz.zcu.tlinhart.lasergame.server.utils.RandomString;
import cz.zcu.tlinhart.lasergame.server.utils.Utils;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * LaserGame manager of logged in users.
 */
@Component
@Scope(value = "singleton")
public class SessionManager {
    private static SessionManager instance;

    private Map<String, User> loggedInSessions;

    //Autowired values

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    JwtTokenUtil tokenUtil;

    @Autowired
    private UserRepository repository;

    // Beans

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(properties.getPasswordHashStrength());
    }

    // Constructor
    private SessionManager() {
        //Singleton private constructor
    }

    @PostConstruct
    private SessionManager initializeInstance() {
        instance = this;
        loggedInSessions = new HashMap<>();
        return this;
    }

    public static SessionManager getInstance() {
        return instance;
    }

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************

    /**
     * Creates SESSIONID for user with given ID.
     *
     * @param user user login information
     * @param userID ID of the user to log in
     * @return SESSIONID
     * @throws NotLogoverException if user wants to log in but is logged in at other terminal.
     */
    public String loginUser(UserLoginBody user, Long userID) throws NotLogoverException {
        if (isLoggedInByUsername(user.getUsername())) {
            if (!user.getLogover()) {
                throw new NotLogoverException();
            } else {
                logoutUserByUsername(user.getUsername());
            }
        }

        String session = generateSessionID();

        User modelUser = new User();
        modelUser.setUsername(user.getUsername());
        modelUser.setId(userID);


        loggedInSessions.put(session, modelUser);
        return session;
    }

    /**
     * Logs out user with given username
     * @param username of the user to logout.
     */
    public void logoutUserByUsername(String username) {
        String sessionID = findSessionByUsername(username);

        if (sessionID != null) {
            this.loggedInSessions.remove(sessionID);
        }
    }

    /**
     * Registers new user with given username and password
     * @param username username
     * @param password password
     * @return true if registered successfully
     */
    public boolean registerUser(String username, String password) {
        if (Utils.isBlank(username) || Utils.isBlank(password)) {
            return false;
        }

        if (repository.existsByUsername(username)) {
            return false;
        }

        String hashedPassword = passwordEncoder().encode(password);

        User newUser = User.builder()
                .username(username)
                .password(hashedPassword)
                .build();

        repository.saveAndFlush(newUser);

        return true;
    }

    /**
     * Returns user instance by his JWT Token. May return null if user is not logged in.
     * @param JWTToken User authentication token
     * @return Instance of User
     */
    public User getUserByJWTToken(String JWTToken) {
        String sessionID = tokenUtil.getSessionIDFromToken(JWTToken);

        return loggedInSessions.get(sessionID);
    }

    /**
     * Logs out user with given JWT Token.
     *
     * @param JWTToken User authentication token
     */
    public void logoutUserByJWTToken(String JWTToken) {
        String sessionID = tokenUtil.getSessionIDFromToken(JWTToken);

        this.loggedInSessions.remove(sessionID);
    }

    /**
     * Validates if user identified by given JWT Token is logged in. For example after restarting the application no user is logged in,
     * yet the non expired JWT token is still valid. Also the application enables only one instance per user - Application logs out any other
     * tokens when Logover attribute is specified on request, but the JWT token is still valid. Method uses SessionID from the JWT Token to validate
     * if the user is actually logged in.
     *
     * @param JWTToken JWT Token
     * @return Returns true is user is logged in, false otherwise
     */
    public boolean isLoggedIn(String JWTToken) {
        String sessionID = tokenUtil.getSessionIDFromToken(JWTToken);

        return loggedInSessions.containsKey(sessionID);
    }

    //********************************************************
    //                  PRIVATE METHODS
    //********************************************************

    private String generateSessionID() {
        String session;
        do {
            session = new RandomString().nextString();
        } while (loggedInSessions.containsKey(session));

        return session;
    }

    private String findSessionByUsername(String username) {
        for (String sessionID : loggedInSessions.keySet()) {
            if (loggedInSessions.get(sessionID).getUsername().equals(username)) {
                return sessionID;
            }
        }

        return null;
    }

    private boolean isLoggedInByUsername(String username) {
        return findSessionByUsername(username) != null;
    }
}

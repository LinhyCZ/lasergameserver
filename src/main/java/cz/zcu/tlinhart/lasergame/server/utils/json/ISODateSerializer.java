package cz.zcu.tlinhart.lasergame.server.utils.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Serializer to ISO Date format.
 * 
 * @author hors
 */
public class ISODateSerializer extends StdSerializer<OffsetDateTime> {
	public static final String ISO_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

	private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ISO_DATETIME_FORMAT);

	/**
	 * Basic constructor
	 */
    public ISODateSerializer() {
        this(null);
    }

    /**
     * Basic constructor with class parameter
     * 
     * @param pDateClass - class parameter
     */
    public ISODateSerializer(Class<OffsetDateTime> pDateClass) {
        super(pDateClass);
    }

	@Override
	public void serialize(OffsetDateTime pValue, JsonGenerator pGenerator, SerializerProvider pProvider) throws IOException {
		pGenerator.writeString(pValue.format(dateFormatter));
	}
}

package cz.zcu.tlinhart.lasergame.server.socketServer;

import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import java.time.OffsetDateTime;
import java.util.List;
import lombok.SneakyThrows;

/**
 * Daemon to loop through SocketThreads and resend messages if they haven't been ACKed yet
 */
public class Acknowledger extends Thread {
    private static Acknowledger instance;

    private final ApplicationProperties properties;

    private boolean isRunning = true;

    private Acknowledger(ApplicationProperties properties) {
        this.properties = properties;
    }

    /**
     * Initialize Acknowledger
     * @param properties Application properties
     * @return Acknowledger instance.
     */
    public static Acknowledger initialize(ApplicationProperties properties) {
        if (instance == null) {
            instance = new Acknowledger(properties);
        }

        return instance;
    }

    public static Acknowledger getInstance() {
        return instance;
    }

    /**
     * Worker thread of the Acknowledger. Resends the message if acknowledge timeout expired.
     */
    @SneakyThrows
    @Override
    public void run() {
        while (isRunning) {
            synchronized (SocketServer.getInstance().getSocketThreadsLock()) {
                List<SocketThread> threads = SocketServer.getInstance().getClientThreads();
                OffsetDateTime now = OffsetDateTime.now();

                if (threads != null) {
                    for (SocketThread thread : threads) {
                        if (thread.isClosed()) {
                            //Remove client threads in case something on client went wrong.
                            SocketServer.getInstance().stoppedClientThread(thread);

                            //We don't care about this socket anymore, we can't send messages to it anyway.
                            continue;
                        }

                        SocketMessage messageInBuffer = thread.getMessageInSentBuffer();
                        if (messageInBuffer != null) {
                            OffsetDateTime messageAcknowledgeTimeout = messageInBuffer.getSendTime().plusSeconds(properties.getMessageAcknowledgeTimeoutSeconds());

                            if (messageAcknowledgeTimeout.isBefore(now)) {
                                thread.resendMessageInSentBuffer();
                            }
                        }
                    }
                }
            }

            Thread.sleep(properties.getAcknowledgerCheckPeriod() * 1000L);
        }
    }

    /**
     * Stops the acknowledger
     */
    public void stopAcknowledger() {
        isRunning = false;
    }
}

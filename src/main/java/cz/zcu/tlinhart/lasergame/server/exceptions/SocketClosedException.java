package cz.zcu.tlinhart.lasergame.server.exceptions;

public class SocketClosedException extends SocketException {
    public SocketClosedException() {
        super("Cannot send messages to closed socket.");
    }
}

package cz.zcu.tlinhart.lasergame.server.socketServer;

/**
 * Enum of commands that contain only command and no data.
 */
public enum SimpleSocketCommand implements ISendable{
    LOGINOK, LOGINERR, STOP, UNKNOWN_COMMAND;

    @Override
    public String getSocketSendData() {
        return this.name();
    }
}

package cz.zcu.tlinhart.lasergame.server.rest.controller;

import cz.zcu.tlinhart.lasergame.server.rest.api.DesktopApi;
import cz.zcu.tlinhart.lasergame.server.rest.model.SetCurrentPageBody;
import cz.zcu.tlinhart.lasergame.server.rest.service.DesktopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for processing /api/desktop endpoints. More information in service-api.yaml
 */
@RestController
public class DesktopController implements DesktopApi {

    @Autowired
    private DesktopService service;

    @Override
    public ResponseEntity<Void> apiSetCurrentPagePost(String authorization, SetCurrentPageBody setCurrentPageBody) throws Exception {
        service.saveCurrentPage(authorization, setCurrentPageBody);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

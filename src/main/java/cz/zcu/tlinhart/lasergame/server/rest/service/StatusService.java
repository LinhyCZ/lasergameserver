package cz.zcu.tlinhart.lasergame.server.rest.service;

import cz.zcu.tlinhart.lasergame.server.rest.model.StatusBody;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import javax.management.JMException;
import javax.management.ObjectName;
import org.springframework.stereotype.Service;

/**
 * Service for processing /api/status endpoints. More information in service-api.yaml
 */
@Service
public class StatusService {

    private static final BigDecimal UNKNOWN_CPULOAD = BigDecimal.valueOf(-1);

    public StatusBody getApplicationStatus() {
        StatusBody status = new StatusBody();

        status.cpuUsage(BigDecimal.valueOf(ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage()))
                .heapUsage(BigDecimal.valueOf(ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed()))
                .nonHeapUsage(BigDecimal.valueOf(ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed()))
                .uptime(BigDecimal.valueOf(ManagementFactory.getRuntimeMXBean().getUptime() / 1000.0));

        try {
            double cpuLoad = (Double)ManagementFactory.getPlatformMBeanServer()
                    .getAttribute(new ObjectName("java.lang:type=OperatingSystem"), "SystemCpuLoad");

            status.cpuUsage(BigDecimal.valueOf(cpuLoad));
        } catch (JMException ex) {
            status.cpuUsage(UNKNOWN_CPULOAD);
        }

        return status;
    }
}

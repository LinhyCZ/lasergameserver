package cz.zcu.tlinhart.lasergame.server.game.games;

import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessageIDException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadException;
import cz.zcu.tlinhart.lasergame.server.exceptions.SocketClosedException;
import cz.zcu.tlinhart.lasergame.server.exceptions.StartGameException;
import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.GameActionFactory;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.PlayerPosition;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.repository.GameRepository;
import cz.zcu.tlinhart.lasergame.server.repository.PlayerPositionRepository;
import cz.zcu.tlinhart.lasergame.server.repository.PlayerRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameAction;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartPlayerData;
import cz.zcu.tlinhart.lasergame.server.socketServer.SimpleSocketCommand;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketMessageAction;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketThread;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Main class for managing games running on the server. Singleton, it's instance is available by getInstance() method.
 */
@Component
@Scope(value = "singleton")
public class GamesManager {
    private static GamesManager instance;

    private List<Game> runningGames = new ArrayList<>();
    private List<Game> completedGames = new ArrayList<>(); //Will be emptied by the service requesting completed data.

    private final Map<Vest, Game> vestsInGame = new HashMap<>();

    Logger logger = LoggerFactory.getLogger(GamesManager.class);

    @Autowired
    GameRepository repository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    PlayerPositionRepository playerPositionRepository;

    @Autowired
    ApplicationProperties properties;

    private GamesManager() {
        // Singleton constructor
    }

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************

    /**
     * Spring post construct method to save Singleton instance to static variable.
     * @return singleton instance. Can be @Autowired
     */
    @PostConstruct
    private GamesManager initializeInstance() {
        instance = this;
        runningGames = repository.findRunningGames();

        return instance;
    }

    /**
     * Standard getInstance method.
     * @return instance of manager.
     */
    public static GamesManager getInstance() {
        return instance;
    }

    /**
     * Action to create one instance of game
     *
     * @param game instance of newly created game
     */
    public void createGame(Game game) {
        runningGames.add(game);
    }

    /**
     * Action to start one instance of game. Finds correct game by ID in runningGames list.
     *
     * @return Returns instance of currently started game.
     * @param data instance of game to start
     * @throws StartGameException Reason preventing the game to start
     */

    /**
     * Starts new game with given game parameters. Sends PREAPRE and START messages to all vests.
     *
     * @param startUser User that created and started the game.
     * @param data REST API object containing data about the game.
     * @throws Exception Couldn't start game. Reason could be Socket timeout or validation issue.
     */
    public void prepareAndStartGame(User startUser, StartGameData data) throws Exception {
        Game game = getRunningGameByID(data.getGameID());

        if (game == null) {
            throw new Exception("Couldn't find game with this ID");
        }

        if (!game.getStartUser().equals(startUser)) {
            throw new StartGameException(StartGameException.StartGameExceptionReason.USER_MISMATCH);
        }

        game.getPlayerList().clear();

        for (StartPlayerData playerData : data.getPlayerData()) {
            Player player = Player.fromRESTData(playerData);
            player.setGame(game);

            playerRepository.saveAndFlush(player);

            game.getPlayerList().add(player);
        }

        game.setGameDuration(data.getDuration());
        game.setPlayerLives(data.getPlayerLives());
        game.setWeaponDamage(data.getWeaponDamage());
        game.setPlayerAmmo(data.getPlayerAmmo());
        game.setRespawnPeriod(data.getRespawnPeriod());
        game.setStartTimeout(data.getStartTimeout());

        //Send PREPARE to all vests.
        startPrepare(game);

        long prepareTimeout = System.currentTimeMillis() + properties.getPrepareResponseTimeout() * 1000L;

        while (prepareTimeout >= System.currentTimeMillis()) {
            if (game.getStatus() == GameSummary.GameStatusEnum.STARTING) {
                break;
            }

            Thread.sleep(100);
        }

        //Validate PREPARE was OK
        if (game.getStatus() != GameSummary.GameStatusEnum.STARTING) {
            game.prepareFailed();
            throw new StartGameException(StartGameException.StartGameExceptionReason.PREPARE_MESSAGE_TIMEOUT);
        }


        //Send START to all vests
        startGame(game);

        long startTimeout = System.currentTimeMillis() + properties.getStartResponseTimeout() * 1000L;
        while (startTimeout >= System.currentTimeMillis()) {
            if (game.getStatus() == GameSummary.GameStatusEnum.RUNNING) {
                break;
            }

            Thread.sleep(100);
        }

        //Validate START was OK
        if (game.getStatus() != GameSummary.GameStatusEnum.RUNNING) {
            stopGame(game);
            throw new StartGameException(StartGameException.StartGameExceptionReason.START_MESSAGE_TIMEOUT);
        }

    }

    /**
     * Action to start prepare of one instance of game
     *
     * @param game instance of game to start
     * @throws Exception Reason preventing the game to start
     */
    public void startPrepare(Game game) throws Exception {
        try {
            game.startPrepare();
            doBeforeGameValidation(game);

            for (Player player : game.getPlayerList()) {
                Vest vest = player.getVest();

                PrepareInformation prepareInformation = game.getGenericPrepareInformation();
                prepareInformation.setCurrentPlayer(player);

                vest.getSocket().send(prepareInformation);

                vestsInGame.put(vest, game);
            }

            repository.saveAndFlush(game);
        } catch (Exception e) {
            //Set Game status and rethrow the error.
            game.prepareFailed();
            throw e;
        }
    }

    /**
     * Sends START message to all vests in the game.
     *
     * @param game game to be started
     * @throws Exception couldn't broadcast start game messages.
     */
    public void startGame(Game game) throws Exception {
        try {
            broadcastStartMessage(game);
        } catch (InvalidSocketMessageIDException | SocketClosedException | InvalidSocketMessagePayloadException e) {
            stopGame(game);
            throw e;
        }
    }

    /**
     * Send STOP message to all sockets in game with given gameID.
     *
     * @param gameID ID of game to stop.
     * @throws InvalidSocketMessageIDException Wrong socket message ID.
     * @throws SocketClosedException Socket is not available.
     * @throws InvalidSocketMessagePayloadException Wrong socket message content.
     */
    public void stopGame(Long gameID) throws InvalidSocketMessageIDException, SocketClosedException, InvalidSocketMessagePayloadException {
        stopGame(getRunningGameByID(gameID));
    }

    /**
     * Send STOP message to all sockets in given game.
     *
     * @param game ID of game to stop.
     * @throws InvalidSocketMessageIDException Wrong socket message ID.
     * @throws SocketClosedException Socket is not available.
     * @throws InvalidSocketMessagePayloadException Wrong socket message content.
     */
    public void stopGame(Game game) throws InvalidSocketMessageIDException, SocketClosedException, InvalidSocketMessagePayloadException {
        if (game.getStatus() != GameSummary.GameStatusEnum.COMPLETED) {
            runningGames.remove(game);
            completedGames.add(game);

            if (GameSummary.GameStatusEnum.RUNNING.equals(game.getStatus()) || GameSummary.GameStatusEnum.STARTING.equals(game.getStatus())) {
                for (Player player : game.getPlayerList()) {
                    Vest vest = player.getVest();
                    SocketThread socket = vest.getSocket();

                    if (socket != null) {
                        socket.send(SimpleSocketCommand.STOP);
                    }

                    vestsInGame.remove(vest);

                    //Save all the data to database.
                    playerRepository.saveAndFlush(player);
                }
            }

            game.stop();
            repository.saveAndFlush(game);
        }
    }

    /**
     * Returns list of running games
     * @return list of running games
     */
    public List<Game> getRunningGames() {
        return runningGames;
    }

    /**
     * Returns game by given ID if the start user matches the user given in parameter.
     *
     * @param gameId ID of the game
     * @param user User that started the game
     * @return true if game exists, false otherwise
     */
    public Game getGameByIdForUser(Long gameId, User user) {
        Game game = getRunningGameByID(gameId);

        if (game != null && game.getStartUser().equals(user)) {
            return game;
        }

        return repository.findById(gameId, user);
    }

    /**
     * Returns running game by given ID
     *
     * @param gameId running game ID
     * @return instance of running game.
     */
    public Game getRunningGameByID(Long gameId) {
        for (Game game : runningGames) {
            if (game.getId().equals(gameId)) {
                return game;
            }
        }

        return null;
    }

    /**
     * Returns true if given user has any running games.
     * @param user given user
     * @return true = user has running games; false otherwise
     */
    public boolean hasRunningGames(User user) {
        for (Game game : runningGames) {
            if (game.getStartUser().equals(user)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Action to process socket commands that are related with games.
     *
     * @param socket socket that received the message
     * @param command split command data
     */
    public void processVestCommand(SocketThread socket, String[] command) {
        Game game;
        Vest vest;
        VestManager vestManager = VestManager.getInstance();
        GamesManager gamesManager = GamesManager.getInstance();

        SocketMessageAction action = SocketMessageAction.byValue(command[0]);
        switch (action) {
            case LOGIN:
                if (command.length != 2) {
                    logger.error("Invalid command count for LOGIN command: " + command.length);
                    sendLoginResponseToSocket(socket, false);

                    return;
                }

                String vestCode = command[1];
                vest = vestManager.getVestByCode(vestCode);

                if (vest == null) {
                    logger.error("Couldn't find vest with code: " + vestCode);
                    sendLoginResponseToSocket(socket, false);

                    return;
                }


                if (VestManager.getInstance().getVestStatus(vest) == VestStatus.ONLINE) {
                    logger.error("Vest is already online: " + vestCode);
                    sendLoginResponseToSocket(socket, false);

                    return;
                }

                vest.setSocket(socket);
                VestManager.getInstance().connectVest(vest);

                sendLoginResponseToSocket(socket, true);
                break;
            case PREPAREDONE:
                game = getGameBySocket(socket);

                if (game != null && game.addPreparedVest()) {
                    game.prepareDone();
                    repository.saveAndFlush(game);
                }

                break;
            case STARTDONE:
                game = getGameBySocket(socket);

                if (game != null && game.addStartedVest()) {
                    game.start();
                    repository.saveAndFlush(game);
                }
                break;
            case POS:
                if (command.length != 3) {
                    logger.error("Tried saving position with wrong number of commands: " + command.length);
                    return;
                }

                try {
                    Float latitude = Float.parseFloat(command[1]);
                    Float longitude = Float.parseFloat(command[2]);
                    vest = vestManager.getVestBySocket(socket);
                    game = gamesManager.getGameByVest(vest);

                    if (game != null && GameSummary.GameStatusEnum.RUNNING.equals(game.getStatus())) {
                        Player player = vest.getPlayer();

                        PlayerPosition newPosition = PlayerPosition.builder().player(player).latitude(latitude).longitude(longitude).date(OffsetDateTime.now()).build();

                        playerRepository.saveAndFlush(player);
                        playerPositionRepository.saveAndFlush(newPosition);

                        if (player.getPlayerPositions() == null) {
                            player.setPlayerPositions(new ArrayList<>());
                        }

                        player.getPlayerPositions().add(newPosition);
                    }
                } catch (Exception e) {
                    logger.error("Got error while trying to save player position.", e);
                }
                break;
            case SHOT:
                if (command.length != 2) {
                    logger.error("Tried storing shot message with wrong number of parameters: " + command.length);
                    return;
                }

                int shooterID;
                try {
                    shooterID = Integer.parseInt(command[1]);
                } catch (NumberFormatException e) {
                    logger.error("Couldn't format string " + command[1] + " to playerID");
                    return;
                }

                vest = vestManager.getVestBySocket(socket);
                game = gamesManager.getGameByVest(vest);

                List<Player> playerList = game.getPlayerList();
                if (shooterID >= playerList.size()) {
                    logger.error("Received shot from unknown ID: " + shooterID);
                    return;
                }

                Player shotPlayer = vest.getPlayer();
                Player shooter = playerList.get(shooterID);

                List<GameAction> actions = game.getActions();
                actions.add(GameActionFactory.shotAction(shooter, shotPlayer, actions.size()));
                shooter.addKill();
                shotPlayer.addDeath();

                break;
            case UNKNOWN:
            default:
                try {
                    socket.send(SimpleSocketCommand.UNKNOWN_COMMAND);
                } catch (SocketClosedException | InvalidSocketMessagePayloadException | InvalidSocketMessageIDException e) {
                    logger.error("Couldn't send UNKNOWN_COMMAND message: ", e);
                }
        }
    }

    /**
     * Returns game instance that is associated with given socket.
     *
     * @param socket socket
     * @return game instance.
     */
    public Game getGameBySocket(SocketThread socket) {
        Vest vest = VestManager.getInstance().getVestBySocket(socket);

        if (vest == null) {
            logger.error("Couldn't find vest by given socket.");
            return null;
        }

        Game game = GamesManager.getInstance().getGameByVest(vest);
        if (game == null) {
            logger.error("Couldn't find game by given vest.");
        }

        return game;
    }

    //********************************************************
    //                  PRIVATE METHODS
    //********************************************************

    private Game getGameByVest(Vest vest) {
        return vestsInGame.get(vest);
    }

    private void sendLoginResponseToSocket(SocketThread socket, boolean success) {
        try {
            if (success) {
                socket.send(SimpleSocketCommand.LOGINOK);
                return;
            }

            socket.send(SimpleSocketCommand.LOGINERR);
        } catch (SocketClosedException | InvalidSocketMessagePayloadException | InvalidSocketMessageIDException e) {
            logger.error("Couldn't send LOGINOK/LOGINERR to Socket: ", e);
        }
    }

    private void broadcastStartMessage(Game game) throws InvalidSocketMessageIDException, SocketClosedException, InvalidSocketMessagePayloadException {
        StartInformation startInformation = game.getStartInformation();
        for (Player player : game.getPlayerList()) {
            Vest vest = player.getVest();
            SocketThread socket = vest.getSocket();

            socket.send(startInformation);
        }
    }

    /**
     * Does validation before the game can be started.
     *
     * @param game instance of game to start
     * @throws StartGameException if one of prerequisites fails
     */
    private void doBeforeGameValidation(Game game) throws StartGameException {
        if (!allPlayersHaveVestsAssigned(game)) {
            throw new StartGameException(StartGameException.StartGameExceptionReason.PLAYER_WITHOUT_VEST);
        }

        if (game.getPlayerList().size() < 2) {
            throw new StartGameException(StartGameException.StartGameExceptionReason.TOO_FEW_PLAYERS);
        }

        if (game.getPlayerList().size() > 8) {
            throw new StartGameException(StartGameException.StartGameExceptionReason.TOO_MANY_PLAYERS);
        }

        for (Player player : game.getPlayerList()) {
            if (player.getPlayerName().length() > 10) {
                throw new StartGameException(StartGameException.StartGameExceptionReason.USERNAME_TOO_LONG);
            }

            boolean allLetters = player.getPlayerName().chars().allMatch(Character::isLetterOrDigit);

            if (!allLetters) {
                throw new StartGameException(StartGameException.StartGameExceptionReason.USERNAME_CONTAINS_WRONG_CHARS);
            }
        }



        List<Vest> offlineVests = validateAllVestsOnline(game);

        if (!offlineVests.isEmpty()) {
            String offlineVestsNames = offlineVests.stream().map(Vest::getVestName).collect(Collectors.joining(", "));
            throw new StartGameException(StartGameException.StartGameExceptionReason.VEST_NOT_ONLINE.additionalData("Vesty: " + offlineVestsNames));
        }
    }

    private boolean allPlayersHaveVestsAssigned(Game game) throws StartGameException {
        List<Vest> alreadyAssignedVests = new ArrayList<>();
        for (Player p : game.getPlayerList()) {
            if (p.getVest() == null) {
                return false;
            }

            if (alreadyAssignedVests.contains(p.getVest())) {
                throw new StartGameException(StartGameException.StartGameExceptionReason.PLAYERS_WITH_SAME_VEST);
            }

            alreadyAssignedVests.add(p.getVest());
        }

        return true;
    }

    private List<Vest> validateAllVestsOnline(Game game) {
        List<Vest> offlineVests = new ArrayList<>();

        for (Player p : game.getPlayerList()) {
            Vest vest = p.getVest();

            if (vest != null && vest.getStatus() == VestStatus.OFFLINE) {
                offlineVests.add(vest);
            }
        }

        return offlineVests;
    }

    /**
     * Finds game that is running for given user. Every user can have only one game running at the time.
     *
     * @param user User that started the game.
     * @return Instance of the running game. May be null if no game is found.
     */
    public Game getRunningGameForUser(User user) {
        for (Game game: runningGames) {
            if (game.getStartUser().equals(user)) {
                return game;
            }
        }

        return null;
    }

    /**
     * Finds a game that recently completed to get all actions showed up. While returning the value, it also deletes the value from list.
     *
     * @param user User that started the game
     * @return instance of recently completed game.
     */
    public Game getCompletedGameForUser(User user) {
        for (Game game: completedGames) {
            if (game.getStartUser().equals(user)) {
                completedGames.remove(game);
                return game;
            }
        }

        return null;
    }
}

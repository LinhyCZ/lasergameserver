package cz.zcu.tlinhart.lasergame.server.exceptions;

public enum InvalidSocketMessagePayloadExceptionType {
    NULL, INVALID_CHAR
}

package cz.zcu.tlinhart.lasergame.server.repository;

import cz.zcu.tlinhart.lasergame.server.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Hibernate repository to access player data in database
 */
@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
}

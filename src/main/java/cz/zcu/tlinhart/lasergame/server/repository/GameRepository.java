package cz.zcu.tlinhart.lasergame.server.repository;

import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Hibernate repository to access game data in database
 */
@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
    @Query("FROM Game WHERE startUser = :user ORDER BY ID DESC")
    List<Game> findByUser(@Param("user") User user);

    @Query("FROM Game WHERE startUser = :user AND ID = :id")
    Game findById(@Param("id") Long cloneGameId, @Param("user") User user);

    @Query("FROM Game WHERE Status != 'COMPLETED'")
    List<Game> findRunningGames();
}

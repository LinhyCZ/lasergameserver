package cz.zcu.tlinhart.lasergame.server.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zcu.tlinhart.lasergame.server.rest.api.GamesApi;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameDetailedSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameID;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameInformationRequestBody;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameStartException;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameRequestBody;
import cz.zcu.tlinhart.lasergame.server.rest.service.GamesService;
import cz.zcu.tlinhart.lasergame.server.utils.Utils;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for processing /api/games endpoints. More information in service-api.yaml
 */
@RestController
public class GamesController implements GamesApi {

    @Autowired
    protected ObjectMapper jsonMapper;

    @Autowired
    GamesService service;

    @Autowired
    HttpServletResponse response;

    Logger logger = LoggerFactory.getLogger(GamesController.class);

    @Override
    public ResponseEntity<StartGameData> apiGamesCreateGamePost(String authorization, StartGameRequestBody startGameRequestBody) throws Exception {
        StartGameData startGameData = service.createGame(authorization, startGameRequestBody);

        if (startGameData == null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(startGameData, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> apiGamesDeleteGameDelete(String authorization, GameID gameID) throws Exception {
        HttpStatus responseStatus = service.deleteGame(authorization, gameID);

        return new ResponseEntity<>(responseStatus);
    }

    @Override
    public ResponseEntity<GameDetailedSummary> apiGamesGetGameInfoPost(String authorization, GameInformationRequestBody gameInformationRequestBody) throws Exception {
        HttpStatus responseStatus = HttpStatus.OK;

        GameDetailedSummary summary = service.getDetailedSummary(authorization, gameInformationRequestBody);

        if (summary == null) {
            responseStatus = HttpStatus.CONFLICT;
        }

        return new ResponseEntity<>(summary, responseStatus);
    }

    @Override
    public ResponseEntity<List<GameSummary>> apiGamesMyGamesGet(String authorization) throws Exception {
        List<GameSummary> games = service.getMyGames(authorization);

        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> apiGamesStartGamePost(String authorization, StartGameData startGameData) throws Exception {
        GameStartException exception = service.startGame(authorization, startGameData);

        if (exception != null) {
            Utils.resetAndPrepareResponse(response);
            response.setStatus(409);
            response.getWriter().println(jsonMapper.writeValueAsString(exception));
            return null;
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> apiGamesStopGameGet(String authorization) throws Exception {
        HttpStatus response = HttpStatus.OK;

        try {
            service.stopGameForUser(authorization);
        } catch (Exception e) {
            logger.error("Couldn't stop the game.", e);
            response = HttpStatus.CONFLICT;
        }

        return new ResponseEntity<>(response);
    }
}

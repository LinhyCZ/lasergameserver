package cz.zcu.tlinhart.lasergame.server.model;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.rest.model.GamePlayerData;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartPlayerData;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Simple class for storing information about players. Also used for transfering to database.
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@Table(name = "Player_Results", schema = "laser_game")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Player_Results_ID_Sequence")
    @SequenceGenerator(name = "Player_Results_ID_Sequence", sequenceName = "laser_game.Player_Results_ID_Sequence", allocationSize = 1)
    private Long id;

    @Column(name = "Team")
    @Enumerated(EnumType.STRING)
    private StartPlayerData.TeamEnum team;

    @Column(name = "Player_Name")
    private String playerName;

    @Column(name = "Kills")
    private int kills;

    @Column(name = "Deaths")
    private int deaths;

    @ManyToOne
    @JoinColumn(name = "Vest")
    private Vest vest;

    @ManyToOne
    @JoinColumn(name = "Game")
    private Game game;

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    @OrderBy("date DESC")
    private List<PlayerPosition> playerPositions = new ArrayList<>();

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************
    /**
     * Creates instance of current player
     *
     * @param team Team of the player
     * @param playerName Name of the player
     * @param vest Vest that the player is wearing
     */
    public Player(StartPlayerData.TeamEnum team, Vest vest, String playerName) {
        this.team = team;
        this.playerName = playerName;
        this.vest = vest;
    }

    /**
     * Adds one kill to this player
     */
    public void addKill() {
        this.kills += 1;
    }

    /**
     * Adds one death to this player
     */
    public void addDeath() {
        this.deaths += 1;
    }

    /**
     * Returns how many other players this player shot
     *
     * @return int
     */
    public int getKills() {
        return this.kills;
    }


    /**
     * Returns how many times this player has been shot by other players
     *
     * @return int
     */
    public int getDeaths() {
        return this.deaths;
    }

    /**
     * Returns the vest that the player is wearing
     *
     * @return Vest that the player has assigned.
     */
    public Vest getVest() {
        return this.vest;
    }

    /**
     * Returns team of the player
     *
     * @return Team
     */
    public StartPlayerData.TeamEnum getTeam() {
        return this.team;
    }

    /**
     * Returns new player that has vest, playerName and Team assigned from this player.
     *
     * @return Clone of this player.
     */
    public Player clonePlayer() {
        Player player = new Player();

        player.vest = this.vest;
        player.playerName = this.playerName;
        player.team = this.team;

        return player;
    }

    /**
     * Creates new instance of player from REST data object
     *
     * @param restData REST API data object
     * @return instance of Player object with information from the REST object.
     */
    public static Player fromRESTData(StartPlayerData restData) {
        Player p = new Player();

        p.setPlayerName(restData.getPlayerName());
        p.setTeam(restData.getTeam());

        Vest vest = VestManager.getInstance().getVestByCode(restData.getVestCode());
        p.setVest(vest);
        vest.setPlayer(p);

        return p;
    }

    /**
     * Create REST API GamePlayerData object with information filled from current instance of Player
     *
     * @return Filled REST API GamePlayerData object
     */
    public GamePlayerData generateGamePlayerData() {
        GamePlayerData data = new GamePlayerData();

        if (this.getPlayerPositions().size() == 0) {
            data.setPositions(new ArrayList<>());
        } else {
            for (PlayerPosition position : this.getPlayerPositions()) {

                data.addPositionsItem(new cz.zcu.tlinhart.lasergame.server.rest.model.PlayerPosition()
                        .latitude(position.getLatitude())
                        .longitude(position.getLongitude())
                        .date(position.getDate()));

            }
        }

        data.setPlayerName(this.getPlayerName());
        data.setKills(this.getKills());
        data.setDeaths(this.getDeaths());
        data.setTeam(GamePlayerData.TeamEnum.fromValue(this.getTeam().getValue())); //Use this weird structure because OpenAPI generator creates two identical enums.
        data.setVestCode(this.getVest().getVestCode());

        return data;
    }
}

package cz.zcu.tlinhart.lasergame.server;

import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketServer;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;

/**
 * Entry point of the application.
 */
@SpringBootApplication
public class ServerMain extends SpringBootServletInitializer {
    private static final String COMMAND_DELIMITER = " ";
    private static final String QUIT_COMMAND = "quit";
    private static final String REGISTER_COMMAND = "register";
    private static final String SETSTATUS_COMMAND = "setstatus";
    private static final String LOGOUT_COMMAND = "logout";
    private static final String HELP_COMMAND = "help";
    private static final Class<ServerMain> serverMainClass = ServerMain.class;

    private static ConfigurableApplicationContext ctx;

    private static final Logger logger = LoggerFactory.getLogger(ServerMain.class);


    @Autowired
    private ApplicationProperties properties;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(serverMainClass);
    }

    /**
     * Starts Socket server after Spring application startup
     */
    @EventListener(ApplicationReadyEvent.class)
    public void startSocketServerAfterSpringStartup() {
        SocketServer.startServer(properties);
    }


    /**
     * Starts Spring application
     * @param args Startup arguments
     */
    public static void main(String[] args) {
        //Start spring application
        ctx = SpringApplication.run(serverMainClass, args);

        logger.info("Application is initialized.");

        printWelcome();

        readCommands();
    }

    /**
     * Reads commands from console. Used to register users and debug.
     */
    private static void readCommands() {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine().trim();

        while (!line.equalsIgnoreCase(QUIT_COMMAND)) {
            try {
                String[] splitCommand = line.split(COMMAND_DELIMITER);
                int numberOfArgs = splitCommand.length;

                if (numberOfArgs > 0) {
                    switch (splitCommand[0]) {
                        case REGISTER_COMMAND:

                            if (numberOfArgs == 3) {
                                if (SessionManager.getInstance().registerUser(splitCommand[1], splitCommand[2])) {
                                    System.out.println("Register successful!");
                                } else {
                                    System.out.println("Register not successful!");
                                }

                            } else {
                                System.out.println("Couldn't register user - Wrong number of arguments specified (Got " + (numberOfArgs - 1) + ", Expected 2)");
                            }

                            break;

                        case LOGOUT_COMMAND:
                            if (numberOfArgs == 2) {
                                SessionManager.getInstance().logoutUserByUsername(splitCommand[1]);
                            }

                            break;

                        case SETSTATUS_COMMAND:
                            if (numberOfArgs == 3) {
                                if ("OFFLINE".equals(splitCommand[2])) {
                                    VestManager.getInstance().disconnectVestByCode(splitCommand[1]);
                                } else if ("ONLINE".equals(splitCommand[2])) {
                                    VestManager.getInstance().connectVestByCode(splitCommand[1]);
                                }
                            }
                            break;

                        case HELP_COMMAND:
                        default:
                            printHelp();
                            break;

                    }
                    line = scanner.nextLine().trim();
                } else {
                    System.out.println("Couldn't parse command " + line);
                }
            } catch (Exception e) {
                line = scanner.nextLine().trim();
                e.printStackTrace();
            }
        }

        ctx.stop();
        SocketServer.getInstance().stopServer();

        System.out.println("Thank you for using LaserGame server :)");
        System.exit(0);
    }

    /**
     * Prints welcome message
     */
    public static void printWelcome() {
        System.out.println("Hello! Welcome to LaserGame server.");
        printHelp();
    }

    /**
     * Prints help containing available commands
     */
    public static void printHelp() {
        System.out.println("Available commands: ");
        System.out.println("help - Show this help message");
        System.out.println("register [username] [password] - Register new user");
        System.out.println("logout [username] - Force logout a user");
        System.out.println("setstatus [vestCode] [ONLINE|OFFLINE] - Force set status of a vest");
        System.out.println("quit - Quit the application");
    }
}

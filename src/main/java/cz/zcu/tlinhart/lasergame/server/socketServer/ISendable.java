package cz.zcu.tlinhart.lasergame.server.socketServer;

/**
 * Interface for objects that are meant to be sent by Socket.
 */
public interface ISendable {
    public String getSocketSendData();
}

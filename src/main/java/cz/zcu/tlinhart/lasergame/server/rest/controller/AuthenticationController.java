package cz.zcu.tlinhart.lasergame.server.rest.controller;

import cz.zcu.tlinhart.lasergame.server.exceptions.NotLogoverException;
import cz.zcu.tlinhart.lasergame.server.rest.api.UserManagementApi;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.UserLoginBody;
import cz.zcu.tlinhart.lasergame.server.rest.service.AuthenticationService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for processing /authentication endpoints. More information in service-api.yaml
 */
@RestController
public class AuthenticationController implements UserManagementApi {

    @Autowired
    private AuthenticationService service;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private HttpServletRequest request;

    private Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Override
    public ResponseEntity<LoginResponse> authenticatePost(UserLoginBody userLoginBody) throws Exception {
        LoginResponse token = service.logonUser(userLoginBody);

        return new ResponseEntity<>(token, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleAllExceptions(Exception pException) {
        if (pException instanceof NotLogoverException) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        logger.error("Exception from authentication controller: ", pException);

        return new ResponseEntity<>(pException.getLocalizedMessage(), HttpStatus.CONFLICT);
    }


    @Override
    public ResponseEntity<Void> logoffGet(String authorization) throws Exception {
        service.logoffUser(authorization, request, response);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

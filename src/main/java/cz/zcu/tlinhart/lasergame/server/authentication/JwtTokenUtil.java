package cz.zcu.tlinhart.lasergame.server.authentication;

import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * Util class to save and load values to and from the JWT token.
 */
@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -2550185165626007488L;

    @Autowired
    private ApplicationProperties properties;

    @Value("${jwt.secret}")
    private String secret;

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************

    /**
     * Returns username from given login response
     *
     * @param token LoginResponse containing token
     * @return username
     */
    public String getUsernameFromLoginResponse(LoginResponse token) {
        return getUsernameFromToken(token.getToken());
    }

    /**
     * Returns username stored in the JWT token
     *
     * @param token JWT Token
     * @return username
     */
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    /**
     * Returns expiration date from token
     *
     * @param token JWT token
     * @return expiration date
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * Returns SESSIONID from given JWT token
     *
     * @param token to read session id from
     * @return SESSIONID
     */
    public String getSessionIDFromToken(String token) {
        return getAllClaimsFromToken(token).get("SessionID", String.class);
    }

    /**
     * Returns true if given token is expired
     *
     * @param token to check
     * @return true = is expired, false otherwise
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }


    /**
     * Creates token with given details
     *
     * @param userDetails details of the token
     * @return JWT token
     */
    public String generateToken(SessionUser userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("SessionID", userDetails.sessionId);

        return doGenerateToken(claims, userDetails.getUsername());
    }

    /**
     * Validate the token
     *
     * @param token to validate
     * @param userDetails user information
     * @return true = is valid, false = not valid
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token) && SessionManager.getInstance().isLoggedIn(token));
    }

    //********************************************************
    //                  PRIVATE METHODS
    //********************************************************

    //while creating the token -
    //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    //2. Sign the JWT using the HS512 algorithm and secret key.
    //3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    //   compaction of the JWT to a URL-safe string
    private String doGenerateToken(Map<String, Object> claims, String subject) {
        long JWT_TOKEN_VALIDITY = properties.getSessionTimeoutHours() * 60 * 60L;
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    //for retrieveing any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(removeBearerKeyword(token)).getBody();
    }

    private String removeBearerKeyword(String token) {
        return token.replaceFirst("Bearer ", "");
    }

    private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }
}

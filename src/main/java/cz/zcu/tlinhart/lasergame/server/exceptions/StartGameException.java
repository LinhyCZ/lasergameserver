package cz.zcu.tlinhart.lasergame.server.exceptions;

public class StartGameException extends Exception{
    StartGameExceptionReason reason;

    public StartGameException(StartGameExceptionReason reason) {
        this.reason = reason;
    }

    @Override
    public String getMessage() {
        return reason.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return reason.getMessage();
    }

    public enum StartGameExceptionReason {
        USERNAME_CONTAINS_WRONG_CHARS("Jedno z uživatelských jmen obsahuje nepovolené znaky. Povolené jsou jen písmena a číslice"),
        USERNAME_TOO_LONG("Jedno z uživatelských jmen je příliš dlouhé. Maximální délka je 10 znaků."),
        TOO_MANY_PLAYERS("Příliš mnoho hráčů. Může hrát pouze 8 hráčů."),
        TOO_FEW_PLAYERS("Příliš málo hráčů. Musí hrát nejméně 2 hráči."),
        PLAYERS_WITH_SAME_VEST("Alespoň dva hráči mají nastavenou stejnou vestu."),
        USER_MISMATCH("Uživatel se pokouší spustit hru jiného hráče."),
        START_MESSAGE_TIMEOUT("Jedna nebo více vest neodpověděly na START zprávu."),
        PREPARE_MESSAGE_TIMEOUT("Jedna nebo více vest neodpověděly na PREPARE zprávu."),
        PLAYER_WITHOUT_VEST("Jden nebo více hráčů nemá přiřazenou vestu."),
        VEST_NOT_ONLINE("Jedna nebo více vest nejsou online.");

        private String message;
        private String additionalData;

        StartGameExceptionReason(String message) {
            this.message = message;
        }

        public String getMessage() {
            if (additionalData != null) {
                return message + " Additional data: (" + additionalData + ")";
            }

            return message;
        }

        public StartGameExceptionReason additionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }
    }
}

package cz.zcu.tlinhart.lasergame.server.model;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketThread;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO for storing information about vests in database.
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@Table(name = "Vests", schema = "laser_game")
public class Vest {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VestID_Sequence")
    @SequenceGenerator(name = "VestID_Sequence", sequenceName = "laser_game.VestID_Sequence", allocationSize = 1)
    private Long id;

    @Column(name = "Vest_Name")
    private String vestName;

    @Column(name = "Vest_Code")
    private String vestCode;

    @EqualsAndHashCode.Exclude
    @Column(name = "Owner")
    private Long owner;

    @Transient
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Player player;

    @Transient
    private SocketThread socket;

    //********************************************************
    //                  PUBLIC METHODS
    //********************************************************
    /**
     * Returns status if the vest is online or offline
     *
     * @return ONLINE or OFFLINE
     */
    public VestStatus getStatus() {
        return VestManager.getInstance().getVestStatus(this);
    }

    /**
     * Sets current player assigned to this vest.
     *
     * @param player Player assigned to this vest.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }
}

package cz.zcu.tlinhart.lasergame.server.tests.integration.game;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericSocketTest;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GameShootTest extends GenericSocketTest {
    @Test
    public void checkRegisterShotSuccessful() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);
        gameData.setDuration(60L);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);
        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2));

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 60;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the START was successful
        out.println("STARTDONE");
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();

        //Validate the response.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());

        //Let the game to warm up :)
        Thread.sleep(10000);

        int gameActionsSize = game.getActions().size();

        out.println("SHOT_1");

        //Give some time to process the command
        Thread.sleep(500);

        //Validate that the action was added
        assertEquals(gameActionsSize + 1, game.getActions().size(), "Game action was not added");
        assertEquals("Hráč TestPl2 právě zasáhl hráče TestPl1!", game.getActions().get(gameActionsSize).getActionData());

        //Validate players have added kill and death
        Player player = vest1.getPlayer();
        assertEquals(1, player.getDeaths());
        assertEquals(0, player.getKills());

        Player player2 = vest2.getPlayer();
        assertEquals(0, player2.getDeaths());
        assertEquals(1, player2.getKills());



        //Stop the game.
        mockMvc.perform(get("/api/games/stopGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk());

        //Validate that the vests received STOP message after the stop command was received.
        socketLine = in.readLine();
        assertEquals("5_STOP", socketLine);
        socketLine = in2.readLine();
        assertEquals("5_STOP", socketLine);

        //Validate that the game is completed.
        assertEquals(GameSummary.GameStatusEnum.COMPLETED, game.getStatus());
    }

    @AfterEach
    public void tearDown() {
        playerPositionRepository.deleteAll();
        playerPositionRepository.flush();

        playerRepository.deleteAll();
        playerRepository.flush();

        gameRepository.deleteAll();
        gameRepository.flush();

        VestManager.getInstance().deleteAll();
        GamesManager.getInstance().getRunningGames().clear();
    }
}

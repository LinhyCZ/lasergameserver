package cz.zcu.tlinhart.lasergame.server.tests.integration.utils;

import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartPlayerData;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketServer;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketThread;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GenericSocketTest extends GenericIntegrationTest {
    protected String VEST_CODE_1 = "ASDFGHJKLU";
    protected String VEST_NAME_1 = "TestVest1";
    protected String VEST_CODE_2 = "QWERTZUIOP";
    protected String VEST_NAME_2 = "TestVest2";

    protected final String WELCOME_MESSAGE = "1_Welcome to lasergame";
    protected int SOCKET_INTERVAL_BUFFER = 3;
    protected int SOCKET_TIMEOUT = 10000;

    protected Socket clientSocket;
    protected PrintWriter out;
    protected BufferedReader in;

    protected Socket clientSocket2;
    protected PrintWriter out2;
    protected BufferedReader in2;

    @BeforeEach
    public void prepareClientSocket() throws IOException, InterruptedException {
        //Close any other threads from previous tests.
        for (SocketThread thread : SocketServer.getInstance().getClientThreads()) {
            thread.close();
        }

        //Give the app some time to clean up those threads.
        Thread.sleep(200);

        //Prepare socket client.
        trace("Starting the socket client.");
        clientSocket = new Socket("127.0.0.1", properties.getSocketServerPort());
        clientSocket.setSoTimeout(SOCKET_TIMEOUT);

        long start = System.currentTimeMillis();
        while (!clientSocket.isConnected()) {
            if (start + SOCKET_TIMEOUT < System.currentTimeMillis()) {
                fail("Couldn't connect to the socket server.");
            }
        }
        trace("Successfully connected");
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    @AfterEach
    public void closeSocket() throws IOException {
        out.println("bye");
        clientSocket.close();

        if (clientSocket2 != null) {
            out2.println("bye");
            clientSocket2.close();
        }
    }

    public void prepareSecondClientSocket() throws IOException {
        trace("Starting second socket client.");
        clientSocket2 = new Socket("127.0.0.1", properties.getSocketServerPort());
        clientSocket2.setSoTimeout(SOCKET_TIMEOUT);

        long start = System.currentTimeMillis();
        while (!clientSocket2.isConnected()) {
            if (start + SOCKET_TIMEOUT < System.currentTimeMillis()) {
                fail("Couldn't connect to the socket server.");
            }
        }

        trace("Successfully connected");
        out2 = new PrintWriter(clientSocket2.getOutputStream(), true);
        in2 = new BufferedReader(new InputStreamReader(clientSocket2.getInputStream()));
    }

    public void ackWelcome() throws IOException {
        String welcomeResponse = in.readLine();

        assertEquals(WELCOME_MESSAGE, welcomeResponse);

        out.println("ACK_1");
    }

    public void ackWelcome2() throws IOException {
        String welcomeResponse = in2.readLine();

        assertEquals(WELCOME_MESSAGE, welcomeResponse);

        out2.println("ACK_1");
    }

    protected StartGameData generateStartGameData(LoginResponse response) throws Exception {
        MockHttpServletResponse result = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        // Validate the first game is still created
        StartGameData data = jsonMapper.readValue(result.getContentAsString(), StartGameData.class);

        data.addPlayerDataItem(new StartPlayerData().playerName("TestPl1").team(StartPlayerData.TeamEnum.BLUE).vestCode(VEST_CODE_1));
        data.addPlayerDataItem(new StartPlayerData().playerName("TestPl2").team(StartPlayerData.TeamEnum.RED).vestCode(VEST_CODE_2));

        data.setStartTimeout(5);
        data.setDuration(120L);
        data.setPlayerAmmo(30);
        data.setPlayerLives(150);
        data.setRespawnPeriod(30);
        data.setWeaponDamage(15);

        return data;
    }
}

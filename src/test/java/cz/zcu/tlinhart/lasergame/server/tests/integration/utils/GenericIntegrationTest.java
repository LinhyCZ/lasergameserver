package cz.zcu.tlinhart.lasergame.server.tests.integration.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zcu.tlinhart.lasergame.server.ApplicationProperties;
import cz.zcu.tlinhart.lasergame.server.SpringSecurityConfiguration;
import cz.zcu.tlinhart.lasergame.server.authentication.JwtRequestFilter;
import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.repository.GameRepository;
import cz.zcu.tlinhart.lasergame.server.repository.PlayerPositionRepository;
import cz.zcu.tlinhart.lasergame.server.repository.PlayerRepository;
import cz.zcu.tlinhart.lasergame.server.repository.UserRepository;
import cz.zcu.tlinhart.lasergame.server.repository.VestRepository;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {SpringSecurityConfiguration.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class GenericIntegrationTest {
    protected static final String CORRECT_USERNAME = "automatedTestUser";
    protected static final String CORRECT_PASSWORD = "testPassword";

    protected static final String CORRECT_USERNAME2 = "automatedTestUser2";
    protected static final String CORRECT_PASSWORD2 = "testPassword2";

    protected static final String EMPTY_OBJECT = "[]";

    protected MockMvc mockMvc;

    protected String tokenToLogout;

    @Autowired
    protected ObjectMapper jsonMapper;

    @Autowired
    protected JwtRequestFilter jwtRequestFilter;

    @Autowired
    protected WebApplicationContext context;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected VestRepository vestRepository;

    @Autowired
    protected GameRepository gameRepository;

    @Autowired
    protected PlayerRepository playerRepository;

    @Autowired
    protected PlayerPositionRepository playerPositionRepository;

    @Autowired
    protected ApplicationProperties properties;

    @BeforeAll
    public void registerTestingUsers() {
        SessionManager.getInstance().registerUser(CORRECT_USERNAME, CORRECT_PASSWORD);
        SessionManager.getInstance().registerUser(CORRECT_USERNAME2, CORRECT_PASSWORD2);
    }

    @BeforeAll
    public void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilters(jwtRequestFilter).build();
    }

    public LoginResponse login(String username, String password) throws Exception {

        String body = "{\"username\": \"" + username +"\", \"password\":\"" + password + "\", \"logover\": true}";

        MvcResult result = mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        LoginResponse loginResponse = jsonMapper.readValue(result.getResponse().getContentAsString(), LoginResponse.class);
        tokenToLogout = loginResponse.getToken();

        return loginResponse;
    }

    public LoginResponse login() throws Exception {
        return login(CORRECT_USERNAME, CORRECT_PASSWORD);
    }

    public LoginResponse login2() throws Exception {
        return login(CORRECT_USERNAME2, CORRECT_PASSWORD2);
    }

    @AfterAll
    public void tearDown() {
        userRepository.deleteAll();
        userRepository.flush();
    }

    @AfterEach
    public void logoutAfterTest() throws Exception {
        if (tokenToLogout != null) {
            logout(tokenToLogout);
            tokenToLogout = null;
        }
    }

    public void logout(String token) throws Exception {
        mockMvc.perform(get("/logoff")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public MockHttpServletRequestBuilder statusRequest(String token) {
        return get("/api/status")
                        .header("Authorization", token)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON);
    }

    public boolean testGetEndpointForInvalidAuth(String endpoint) throws Exception {
        return testEndpointForInvalidAuth(endpoint, "get");
    }

    public boolean testPostEndpointForInvalidAuth(String endpoint) throws Exception {
        return testEndpointForInvalidAuth(endpoint, "post");
    }

    public boolean testDeleteEndpointForInvalidAuth(String endpoint) throws Exception {
        return testEndpointForInvalidAuth(endpoint, "delete");
    }

    private boolean testEndpointForInvalidAuth(String endpoint, String type) throws Exception {
        MvcResult result = mockMvc.perform(getBuilder(endpoint, type)
                        .header("Authorization", "Bearer invalid")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        if (result.getResponse().getStatus() != HttpServletResponse.SC_UNAUTHORIZED) {
            return false;
        }

        result = mockMvc.perform(getBuilder(endpoint, type)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        if (result.getResponse().getStatus() != HttpServletResponse.SC_UNAUTHORIZED) {
            return false;
        }

        return true;
    }

    private MockHttpServletRequestBuilder getBuilder(String uri, String type) {
        switch (type.toLowerCase()) {
            case "delete":
                return delete(uri);
            case "post":
                return post(uri);
            case "get":
                return get(uri);
        }

        //Default is get
        return get(uri);
    }

    public void trace(String message) {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement e = stacktrace[2];//maybe this number needs to be corrected
        String methodName = e.getMethodName();
        String[] classNameSplit = e.getClassName().split("\\.");
        String className = classNameSplit[classNameSplit.length - 1];

        String completeName = className + "." + methodName;

        Logger logger = LoggerFactory.getLogger(GenericIntegrationTest.class);
        logger.trace("{}: {}", completeName, message);
    }

    public User getTestingUser() {
        return userRepository.findByUsername(CORRECT_USERNAME);
    }

    public User getTestingUser2() {
        return userRepository.findByUsername(CORRECT_USERNAME2);
    }

    public StartGameData createGame(String authToken) throws Exception {
        MockHttpServletResponse result = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", authToken)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        return jsonMapper.readValue(result.getContentAsString(), StartGameData.class);
    }

    public void registerVest(VestRegisterInfo info, LoginResponse response) throws Exception {
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(info)))
                .andExpect(status().isOk());
    }
}

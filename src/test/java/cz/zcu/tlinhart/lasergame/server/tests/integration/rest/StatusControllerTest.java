package cz.zcu.tlinhart.lasergame.server.tests.integration.rest;

import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.StatusBody;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

public class StatusControllerTest extends GenericIntegrationTest {
    @Test
    public void testStatusWithoutLogin() throws Exception {
        assertTrue(testGetEndpointForInvalidAuth("/api/status"));
    }

    @Test
    public void testStatus() throws Exception {
        LoginResponse loginResponse = login();

        MvcResult result = mockMvc.perform(statusRequest(loginResponse.getToken()))
                        .andReturn();
        MockHttpServletResponse response = result.getResponse();

        StatusBody responseObject = jsonMapper.readValue(response.getContentAsString(), StatusBody.class);

        assertEquals(response.getStatus(), HttpStatus.OK.value(), "Wrong HTTP response status.");
        assertNotNull(responseObject, "StatusBody is null");
        assertNotNull(responseObject.getUptime(), "Uptime is not present in the status response");
        assertNotNull(responseObject.getCpuUsage(), "CPU Usage is not present in the status response");
        assertNotNull(responseObject.getHeapUsage(), "Heap usage is not present in the status response");
        assertNotNull(responseObject.getNonHeapUsage(), "Non Heap usage is not present in the status response");
    }
}

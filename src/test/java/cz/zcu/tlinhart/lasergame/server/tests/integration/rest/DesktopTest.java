package cz.zcu.tlinhart.lasergame.server.tests.integration.rest;

import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DesktopTest extends GenericIntegrationTest {
    private static final String SAVED_PAGE = "terminalSettings";

    @Test
    public void testSavingLastpageWithoutLogin() throws Exception {
        testPostEndpointForInvalidAuth("/api/setCurrentPage");
    }

    @Test
    public void testSavingLastpage() throws Exception {
        LoginResponse response = login();

        String requestBody = "{\"currentPage\": \"" + SAVED_PAGE + "\"}";

        mockMvc.perform(post("/api/setCurrentPage")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());

        // Logout and login to see if the last page is in the login response
        logout(response.getToken());

        response = login();

        assertEquals(SAVED_PAGE, response.getLastPage(), "Saved page in response was different from the page that was saved by the endpoint.");
    }
}

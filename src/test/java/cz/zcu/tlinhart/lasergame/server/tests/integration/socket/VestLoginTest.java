package cz.zcu.tlinhart.lasergame.server.tests.integration.socket;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericSocketTest;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class VestLoginTest extends GenericSocketTest {
    String VEST_CODE = "ASDFASDFAF";
    String VEST_NAME = "TestVest1";

    @Test
    public void testCorrectVestLogin() throws Exception {

        LoginResponse response = login();

        //Register new vest
        VestRegisterInfo vestInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        registerVest(vestInfo, response);

        //Acknowledge welcome
        ackWelcome();

        //Send login message
        out.println("LOGIN_" + VEST_CODE);

        //Validate socket response
        String socketResponse = in.readLine();
        assertEquals("2_LOGINOK", socketResponse);

        //Validate the vest is online in the manager.
        Vest vest = VestManager.getInstance().getVestByCode(VEST_CODE);
        assertEquals(VestStatus.ONLINE, vest.getStatus());
    }

    @Test
    public void testIncorrectCodeVestLogin() throws Exception {
        //Acknowledge welcome
        ackWelcome();

        //Send login message - vest is not registered, so we should get LOGINERR message.
        out.println("LOGIN_" + VEST_CODE);

        //Validate socket response
        String socketResponse = in.readLine();
        assertEquals("2_LOGINERR", socketResponse);
    }

    @Test
    public void testIncorrectMessageLogin() throws Exception {
        LoginResponse response = login();

        //Register new vest
        VestRegisterInfo vestInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        registerVest(vestInfo, response);

        //Acknowledge welcome
        ackWelcome();

        //Send login message - vest is registered, but the command is wrong. We should get LOGINERR.
        out.println("LOGIN_" + VEST_CODE + "_GIBBERISH");

        //Validate socket response
        String socketResponse = in.readLine();
        assertEquals("2_LOGINERR", socketResponse);

        //Validate the vest is still offline in the manager.
        Vest vest = VestManager.getInstance().getVestByCode(VEST_CODE);
        assertEquals(VestStatus.OFFLINE, vest.getStatus());
    }

    @AfterEach
    public void cleanUp() {
        VestManager.getInstance().disconnectVestByCode(VEST_CODE);
        VestManager.getInstance().deleteAll();
    }
}

package cz.zcu.tlinhart.lasergame.server.tests.integration;

import cz.zcu.tlinhart.lasergame.server.authentication.JwtTokenUtil;
import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RegisterTest extends GenericIntegrationTest {
    private final String USERNAME = "registerTestUser";
    private final String PASSWORD = "registerTestPassword";

    @Autowired
    private JwtTokenUtil tokenUtil;

    String currentUserToken = null;

    @Test
    public void testSuccessfulRegister() throws Exception {
        boolean result = SessionManager.getInstance().registerUser(USERNAME, PASSWORD);

        // Assert result from DB
        assertTrue(result, "Failed to register user");

        LoginResponse loginResponse = login(USERNAME, PASSWORD);

        // Assert that user can log in
        assertNotNull(loginResponse);
        assertEquals(tokenUtil.getUsernameFromLoginResponse(loginResponse), USERNAME, "Username doesn't match.");
    }

    @Test
    public void testPreventRegisterWithSameUsername() throws Exception {
        boolean result = SessionManager.getInstance().registerUser(USERNAME, PASSWORD);

        // Assert result from DB
        assertTrue(result, "Failed to register user");

        // Try second register with the same credentials
        result = SessionManager.getInstance().registerUser(USERNAME, PASSWORD);

        assertFalse(result, "Managed to register with same username");

        // Try that the user still can log in
        LoginResponse loginResponse = login(USERNAME, PASSWORD);

        // Assert that user can log in
        assertNotNull(loginResponse);
        assertEquals(tokenUtil.getUsernameFromLoginResponse(loginResponse), USERNAME, "Username doesn't match.");
    }

    @Test
    public void testPreventWrongParams() throws Exception {
        boolean result = SessionManager.getInstance().registerUser(USERNAME, null);
        assertFalse(result, "Manged to register with null param");

        result = SessionManager.getInstance().registerUser(null, PASSWORD);
        assertFalse(result, "Manged to register with null param");

        result = SessionManager.getInstance().registerUser(null, null);
        assertFalse(result, "Manged to register with null param");

        result = SessionManager.getInstance().registerUser(" ", "      ");
        assertFalse(result, "Manged to register with null param");
    }

    @AfterEach
    public void unregisterUser() throws Exception {
        //Prevent automatic logout since we will be deleting the user, and we would fail on exception since the user won't exist while logging out.
        tokenToLogout = null;

        //We log him out here.
        if (currentUserToken != null) {
            logout(currentUserToken);
        }

        User user = userRepository.findByUsername(USERNAME);

        if (user != null) {
            userRepository.deleteById(user.getId());
            userRepository.flush();
        }
    }
}

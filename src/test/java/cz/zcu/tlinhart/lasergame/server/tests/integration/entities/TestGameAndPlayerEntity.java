package cz.zcu.tlinhart.lasergame.server.tests.integration.entities;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartPlayerData;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import java.time.OffsetDateTime;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class TestGameAndPlayerEntity extends GenericIntegrationTest {
    private final String PLAYER_NAME_1 = "Player1";
    private final String PLAYER_NAME_2 = "Player2";

    @Test
    public void doGameAndPlayerEntityTest() {
        User user = getTestingUser();

        OffsetDateTime createTime = OffsetDateTime.now();

        Game game = Game.builder()
                .startUser(user)
                .status(GameSummary.GameStatusEnum.CREATED)
                .startDate(createTime)
                .build();

        gameRepository.saveAndFlush(game);


        Vest vest1 = Vest.builder()
                .owner(user.getId())
                .vestCode("TESTVEST")
                .vestName("Testovaci vesta")
                .build();

        Vest vest2 = Vest.builder()
                .owner(user.getId())
                .vestCode("TESTVEST2")
                .vestName("Testovaci vesta2")
                .build();

        VestManager.getInstance().addVest(vest1);
        VestManager.getInstance().addVest(vest2);

        Player player1 = Player.builder()
                .game(game)
                .deaths(1)
                .kills(1)
                .team(StartPlayerData.TeamEnum.BLUE)
                .playerName(PLAYER_NAME_1)
                .vest(vest1)
                .build();

        Player player2 = Player.builder()
                .game(game)
                .deaths(2)
                .kills(2)
                .team(StartPlayerData.TeamEnum.RED)
                .playerName(PLAYER_NAME_2)
                .vest(vest2)
                .build();

        playerRepository.save(player1);
        playerRepository.save(player2);
        playerRepository.flush();

        List<Game> gamesFromRepository = gameRepository.findByUser(user);

        assertEquals(1, gamesFromRepository.size(), "Didn't find the game for this user");

        Game oneGameFromRepository = gamesFromRepository.get(0);

        List<Player> playerSet = oneGameFromRepository.getPlayerList();
        assertEquals(2, playerSet.size(), "Wrong amount of players in this game.");
    }

    @AfterEach
    public void cleanUp() {
        playerRepository.deleteAll();
        playerRepository.flush();

        VestManager.getInstance().deleteAll();

        gameRepository.deleteAll();
        gameRepository.flush();
    }
}

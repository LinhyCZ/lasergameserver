package cz.zcu.tlinhart.lasergame.server.tests.integration.rest;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.rest.model.*;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GamesControllerTest extends GenericIntegrationTest {
    protected String VEST_CODE_1 = "ASDFGHJKLU";
    protected String VEST_NAME_1 = "TestVest1";
    protected String VEST_CODE_2 = "QWERTZUIOP";
    protected String VEST_NAME_2 = "TestVest2";

    @Test
    public void testSavingLastpageWithoutLogin() throws Exception {
        assertTrue(testGetEndpointForInvalidAuth("/api/games/myGames"));
        assertTrue(testPostEndpointForInvalidAuth("/api/games/createGame"));
        assertTrue(testPostEndpointForInvalidAuth("/api/games/startGame"));
        assertTrue(testGetEndpointForInvalidAuth("/api/games/stopGame"));
        assertTrue(testGetEndpointForInvalidAuth("/api/games/getRunningGameInfo"));
        assertTrue(testPostEndpointForInvalidAuth("/api/games/getFinishedGameInfo"));
        assertTrue(testDeleteEndpointForInvalidAuth("/api/games/deleteGame"));
    }

    @Test
    public void testCreatingGame() throws Exception {
        LoginResponse response = login();

        MockHttpServletResponse result = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result.getStatus(), "Wrong result status");

        StartGameData data = jsonMapper.readValue(result.getContentAsString(), StartGameData.class);

        assertNotNull(data);
        assertNotNull(data.getGameID());

        assertEquals(0, data.getPlayerData().size());

        List<Game> runningGames = GamesManager.getInstance().getRunningGames();
        assertEquals(1, runningGames.size());
        assertEquals(data.getGameID(), runningGames.get(0).getId());
        assertEquals(GameSummary.GameStatusEnum.CREATED, runningGames.get(0).getStatus());
    }

    @Test
    public void testCreatingAndCloningGame() throws Exception {
        LoginResponse response = login();

        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        MockHttpServletResponse result = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result.getStatus(), "Wrong result status");

        StartGameData data = jsonMapper.readValue(result.getContentAsString(), StartGameData.class);

        assertNotNull(data);
        assertNotNull(data.getGameID());

        assertEquals(0, data.getPlayerData().size());

        List<Game> runningGames = GamesManager.getInstance().getRunningGames();
        Game currentGame = runningGames.get(0);
        assertEquals(1, runningGames.size());
        assertEquals(data.getGameID(), currentGame.getId());
        assertEquals(GameSummary.GameStatusEnum.CREATED, currentGame.getStatus());

        //Set as completed by code.
        runningGames.remove(currentGame);
        currentGame.setStatus(GameSummary.GameStatusEnum.COMPLETED);

        //Set game duration and save in DB
        currentGame.setGameDuration(183L);
        currentGame.setPlayerLives(14);
        currentGame.setWeaponDamage(19);
        currentGame.setPlayerAmmo(0);
        currentGame.setRespawnPeriod(196);
        currentGame.setStartTimeout(53);

        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        List<Player> playerList = new ArrayList<>();
        playerList.add(Player.builder().playerName("TestPlayer1").team(StartPlayerData.TeamEnum.BLUE).deaths(2).kills(1).vest(vest1).game(currentGame).build());
        playerList.add(Player.builder().playerName("TestPlayer2").team(StartPlayerData.TeamEnum.RED).deaths(1).kills(8).vest(vest2).game(currentGame).build());
        currentGame.setPlayerList(playerList);

        gameRepository.saveAndFlush(currentGame);

        for (Player p : playerList) {
            playerRepository.saveAndFlush(p);
        }

        //Try cloning the game and validate the game duration was set and is the same.
        StartGameRequestBody startGameRequestBody = new StartGameRequestBody();
        startGameRequestBody.gameID(data.getGameID());

        MockHttpServletResponse result2 = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(startGameRequestBody)))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result2.getStatus(), "Wrong result status");

        StartGameData newGameData = jsonMapper.readValue(result2.getContentAsString(), StartGameData.class);

        assertNotEquals(newGameData.getGameID(), currentGame.getId(), "Got the same game ID");
        assertEquals(currentGame.getGameDuration(), newGameData.getDuration(), "Game duration was not cloned.");
        assertEquals(currentGame.getPlayerLives(), newGameData.getPlayerLives(), "Player Lives was not cloned.");
        assertEquals(currentGame.getWeaponDamage(), newGameData.getWeaponDamage(), "Weapon damage was not cloned.");
        assertEquals(currentGame.getPlayerAmmo(), newGameData.getPlayerAmmo(), "Player ammo was not cloned.");
        assertEquals(currentGame.getRespawnPeriod(), newGameData.getRespawnPeriod(), "Respawn period was not cloned.");
        assertEquals(currentGame.getStartTimeout(), newGameData.getStartTimeout(), "Start Timeout was not cloned.");

        assertEquals(2, currentGame.getPlayerList().size());
        assertEquals(2, newGameData.getPlayerData().size());

        //Assert players are still correctly assigned to their game.
        assertEquals(currentGame.getId(), currentGame.getPlayerList().get(0).getGame().getId());
        assertEquals(currentGame.getId(), currentGame.getPlayerList().get(1).getGame().getId());

        assertEquals(StartPlayerData.TeamEnum.BLUE, newGameData.getPlayerData().get(0).getTeam());
        assertEquals(StartPlayerData.TeamEnum.RED, newGameData.getPlayerData().get(1).getTeam());
        assertEquals("TestPlayer1", newGameData.getPlayerData().get(0).getPlayerName());
        assertEquals("TestPlayer2", newGameData.getPlayerData().get(1).getPlayerName());
    }

    @Test
    public void testCreatingGameAndFetchingAfterwards() throws Exception {
        LoginResponse response = login();

        //Create new game and validate
        MockHttpServletResponse result = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result.getStatus(), "Wrong result status");

        StartGameData data = jsonMapper.readValue(result.getContentAsString(), StartGameData.class);

        assertNotNull(data);
        assertNotNull(data.getGameID());

        assertEquals(0, data.getPlayerData().size());

        List<Game> runningGames = GamesManager.getInstance().getRunningGames();
        assertEquals(1, runningGames.size());
        assertEquals(data.getGameID(), runningGames.get(0).getId());
        assertEquals(GameSummary.GameStatusEnum.CREATED, runningGames.get(0).getStatus());

        StartGameRequestBody startGameRequestBody = new StartGameRequestBody();
        startGameRequestBody.gameID(data.getGameID());
        startGameRequestBody.fetchData(true);

        MockHttpServletResponse result2 = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(startGameRequestBody)))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result2.getStatus(), "Wrong result status");
        assertEquals(result.getContentAsString(), result2.getContentAsString(), "Start game data are not the same");
    }

    @Test
    public void testPreventingCreationOfTwoGames() throws Exception {
        // Create first game
        LoginResponse response = login();

        MockHttpServletResponse result = mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result.getStatus(), "Wrong result status");

        mockMvc.perform(post("/api/games/createGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());

        // Validate the first game is still created
        StartGameData data = jsonMapper.readValue(result.getContentAsString(), StartGameData.class);

        assertEquals(data.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());
    }

    @Test
    public void testDeletingNonExistingGame() throws Exception {
        LoginResponse response = login();

        GameID gameID = new GameID().gameID(1L);

        mockMvc.perform(delete("/api/games/deleteGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(gameID)))
                .andExpect(status().isConflict());
    }

    @Test
    public void testDeletingCreatedGame() throws Exception {
        LoginResponse response = login();
        StartGameData game = createGame(response.getToken());

        //Check that the game is registered in GamesManager
        assertEquals(game.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());

        GameID gameID = new GameID().gameID(game.getGameID());

        mockMvc.perform(delete("/api/games/deleteGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(gameID)))
                .andExpect(status().isOk());

        //Check that the game is not in the manager anymore
        assertEquals(0, GamesManager.getInstance().getRunningGames().size());

        //Check that the game is not in the repository anymore
        Game gameFromRepo = gameRepository.findById(gameID.getGameID(), getTestingUser());
        assertNull(gameFromRepo);
    }

    @Test
    public void testPrevetingOfDeletionOfPreparingAndRunningGame() throws Exception {
        LoginResponse response = login();
        StartGameData startGameData = createGame(response.getToken());

        //Check that the game is registered in GamesManager
        assertEquals(startGameData.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());

        //Check with game in Preparing status
        GameID gameID = new GameID().gameID(startGameData.getGameID());
        Game game = GamesManager.getInstance().getRunningGameByID(gameID.getGameID());
        game.setStatus(GameSummary.GameStatusEnum.PREPARING);
        gameRepository.saveAndFlush(game);
        mockMvc.perform(delete("/api/games/deleteGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(gameID)))
                .andExpect(status().isNotAcceptable());

        //Check that the game is still fine and dandy
        assertEquals(startGameData.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());
        assertEquals(GameSummary.GameStatusEnum.PREPARING, GamesManager.getInstance().getRunningGames().get(0).getStatus());

        //Check game with STARTING status
        game.setStatus(GameSummary.GameStatusEnum.STARTING);
        gameRepository.saveAndFlush(game);
        mockMvc.perform(delete("/api/games/deleteGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(gameID)))
                .andExpect(status().isNotAcceptable());

        //Check that the game is still fine and dandy
        assertEquals(startGameData.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());
        assertEquals(GameSummary.GameStatusEnum.STARTING, GamesManager.getInstance().getRunningGames().get(0).getStatus());

        //Check with game in Running status
        game.setStatus(GameSummary.GameStatusEnum.RUNNING);
        gameRepository.saveAndFlush(game);

        mockMvc.perform(delete("/api/games/deleteGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(gameID)))
                .andExpect(status().isNotAcceptable());

        //Check that the game is still fine and dandy
        assertEquals(startGameData.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());
    }

    @Test
    public void testPreventingOfDeletionOfOtherUsersGame() throws Exception {
        LoginResponse response1 = login();
        LoginResponse response2 = login2();

        StartGameData startGameData = createGame(response1.getToken());
        GameID gameID = new GameID().gameID(startGameData.getGameID());

        //Try deleting with different user.
        mockMvc.perform(delete("/api/games/deleteGame")
                        .header("Authorization", response2.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(gameID)))
                .andExpect(status().isConflict());

        //Check that the game is still fine and dandy and the owner is user1
        assertEquals(startGameData.getGameID(), GamesManager.getInstance().getRunningGames().get(0).getId());
        assertEquals(getTestingUser(), GamesManager.getInstance().getRunningGames().get(0).getStartUser());
    }

    @Test
    public void testSummaryEmpty() throws Exception {
        LoginResponse response = login();

        MockHttpServletResponse result = mockMvc.perform(get("/api/games/myGames")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        assertEquals(EMPTY_OBJECT, result.getContentAsString());
    }

    @Test
    public void testSummaryOneGame() throws Exception {
        LoginResponse response = login();
        StartGameData startGameData = createGame(response.getToken());

        MockHttpServletResponse result = mockMvc.perform(get("/api/games/myGames")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        List<GameSummary> expected = new ArrayList<>();
        expected.add(
                new GameSummary()
                        .gameID(startGameData.getGameID())
                        .gameStatus(GameSummary.GameStatusEnum.CREATED)
                        .numberOfPlayers(0)
        );

        assertEquals(jsonMapper.writeValueAsString(expected), result.getContentAsString());
    }

    @Test
    public void testSummaryTwoGames() throws Exception {
        LoginResponse response = login();
        StartGameData startGameData = createGame(response.getToken());

        //Set game to finished by hand.
        GamesManager.getInstance().stopGame(startGameData.getGameID());

        StartGameData startGameData2 = createGame(response.getToken());

        MockHttpServletResponse result = mockMvc.perform(get("/api/games/myGames")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        //Don't expect start date since we are faking the status.
        //Start date is set when game comes from PREPARING to RUNNING status.

        List<GameSummary> expected = new ArrayList<>();

        expected.add(
                new GameSummary()
                        .gameID(startGameData2.getGameID())
                        .gameStatus(GameSummary.GameStatusEnum.CREATED)
                        .numberOfPlayers(0)
        );

        expected.add(
                new GameSummary()
                        .gameID(startGameData.getGameID())
                        .gameStatus(GameSummary.GameStatusEnum.COMPLETED)
                        .numberOfPlayers(0)
        );


        assertEquals(jsonMapper.writeValueAsString(expected), result.getContentAsString());
    }


    @AfterEach
    public void cleanUp() {
        //cleanup
        playerRepository.deleteAll();
        playerRepository.flush();

        VestManager.getInstance().deleteAll();

        GamesManager.getInstance().getRunningGames().clear();
        gameRepository.deleteAll();
        gameRepository.flush();
    }
}

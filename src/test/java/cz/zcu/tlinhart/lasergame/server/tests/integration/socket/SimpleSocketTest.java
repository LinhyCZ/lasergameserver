package cz.zcu.tlinhart.lasergame.server.tests.integration.socket;

import cz.zcu.tlinhart.lasergame.server.socketServer.SocketServer;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericSocketTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

public class SimpleSocketTest extends GenericSocketTest {

    @Test
    public void simpleTest() throws Exception {
        String response = in.readLine();

        assertEquals(WELCOME_MESSAGE, response, "Response didn't match");

        Integer ID = Integer.valueOf(response.split("_")[0]);

        out.println("ACK_" + ID);

        // Give some time to send the request
        Thread.sleep(200);

        assertNull(SocketServer.getInstance().getClientThreads().get(0).getMessageInSentBuffer());

        out.println("bye");
        clientSocket.close();

        // Give some time to process the disconnect
        Thread.sleep(200);

        assertEquals(0, SocketServer.getInstance().getClientThreads().size(), "Client thread is still in socket server");
    }

    @Test
    public void testAcknowledger() throws Exception {
        String response = in.readLine();

        assertEquals(WELCOME_MESSAGE, response, "Response didn't match");

        int socketTimeout = (properties.getMessageAcknowledgeTimeoutSeconds() + properties.getAcknowledgerCheckPeriod() + SOCKET_INTERVAL_BUFFER) * 1000;
        trace("Setting timeout to " + socketTimeout + "ms");
        // Set timeout so the acknowledger has time to notice the ACK was not sent
        clientSocket.setSoTimeout(socketTimeout);

        // Validate, that the acknowledger resent the message.
        response = in.readLine();
        assertEquals(WELCOME_MESSAGE, response, "Response didn't match");

        // Validate that the message is still in sentBuffer for this socket.
        assertNotNull(SocketServer.getInstance().getClientThreads().get(0).getMessageInSentBuffer());

        //Send acknowledge, validate correct function and close connection
        Integer ID = Integer.valueOf(response.split("_")[0]);

        out.println("ACK_" + ID);

        // Give some time to send the request
        Thread.sleep(200);

        assertNull(SocketServer.getInstance().getClientThreads().get(0).getMessageInSentBuffer());

        out.println("bye");
        clientSocket.close();

        // Give some time to process the disconnect
        Thread.sleep(200);

        assertEquals(0, SocketServer.getInstance().getClientThreads().size(), "Client thread is still in socket server");
    }
    @Test
    public void testMessageBuffer() throws Exception {
        String response = in.readLine();

        assertEquals(WELCOME_MESSAGE, response, "Response didn't match");

        // Force server to send message to currently connected client.
        SocketServer.getInstance().getClientThreads().get(0).send(() -> "Test message");

        // Give some time to process the request
        Thread.sleep(200);

        // Assert that the message in sent buffer is still the welcome message.
        assertEquals(WELCOME_MESSAGE, SocketServer.getInstance().getClientThreads().get(0).getMessageInSentBuffer().toString());

        //Send acknowledge of the message
        Integer ID = Integer.valueOf(response.split("_")[0]);
        out.println("ACK_" + ID);

        // Second test message should be sent.
        response = in.readLine();
        assertEquals("2_Test message", response, "Response didn't match");

        // Validate that the second message is now in sentBuffer for this socket.
        assertEquals("2_Test message", SocketServer.getInstance().getClientThreads().get(0).getMessageInSentBuffer().toString());

        //Send acknowledge, validate correct function and close connection
        ID = Integer.valueOf(response.split("_")[0]);
        out.println("ACK_" + ID);

        // Give some time to send the request
        Thread.sleep(200);

        assertNull(SocketServer.getInstance().getClientThreads().get(0).getMessageInSentBuffer());

        out.println("bye");
        clientSocket.close();

        // Give some time to process the disconnect
        Thread.sleep(200);

        assertEquals(0, SocketServer.getInstance().getClientThreads().size(), "Client thread is still in socket server");
    }
}

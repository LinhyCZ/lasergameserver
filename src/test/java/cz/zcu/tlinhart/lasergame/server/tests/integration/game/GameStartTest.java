package cz.zcu.tlinhart.lasergame.server.tests.integration.game;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameStartException;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericSocketTest;
import java.net.SocketTimeoutException;
import java.time.OffsetDateTime;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GameStartTest extends GenericSocketTest {
    @Test
    public void okGameStartTest() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);
        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2));

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 120;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the START was successful
        out.println("STARTDONE");
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();


        //Validate the response.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());
    }

    @Test
    public void okGameStartTestWithDelay() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);
        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2));

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        Thread.sleep(8500); //Add delay before sending response
        //Send that the PREPARE was successful
        out.println("PREPAREDONE");

        Thread.sleep(3000); //Add delay before sending response
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 120;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        Thread.sleep(9000); //Add delay before sending response
        //Send that the START was successful
        out.println("STARTDONE");

        Thread.sleep(4000); //Add delay before sending response
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();


        //Validate the response.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());
    }

    @Test
    public void gameStartTestCheckOfflineVest() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests - Login only with one vest.
        out.println("LOGIN_" + VEST_CODE_1);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.OFFLINE, VestManager.getInstance().getVestStatus(vest2)); //Vest should be offline in this case.

        //ACK the LOGINOK message
        out.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that no PREPARE or any other message is not sent.
        assertThrows(SocketTimeoutException.class, () -> {
            in.readLine();
        });

        //Validate that the game is back in CREATED state
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());
        assertEquals(GameSummary.GameStatusEnum.CREATED, game.getStatus());

        //Wait for the request to complete.
        requestThread.join();

        //Validate that the response is CONFLICT and contains information.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.CONFLICT.value(), restResponse.getStatus());

        GameStartException exception = new GameStartException().code(GameStartException.CodeEnum.GAME_START_EXCEPTION).detailedInformation("Jedna nebo více vest nejsou online. Additional data: (Vesty: " + VEST_NAME_2 + ")");
        assertEquals(jsonMapper.writeValueAsString(exception).trim(), restResponse.getContentAsString().trim());
    }

    @Test
    public void gameStartTestCheckPrepareFailed() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests - Login only with both vests.
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);

        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2)); //Vest should be offline in this case.

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful - Only from one west
        out.println("PREPAREDONE");

        //Wait for the request to complete.
        requestThread.join();

        //Validate that the game is back in CREATED state
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());
        assertEquals(GameSummary.GameStatusEnum.CREATED, game.getStatus());

        //Validate that the response is CONFLICT and contains information.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.CONFLICT.value(), restResponse.getStatus());

        GameStartException exception = new GameStartException().code(GameStartException.CodeEnum.GAME_START_EXCEPTION).detailedInformation("Jedna nebo více vest neodpověděly na PREPARE zprávu.");
        assertEquals(jsonMapper.writeValueAsString(exception).trim(), restResponse.getContentAsString().trim());
    }

    @Test
    public void gameStartTestCheckStartFailed() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests - Login only with both vests.
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);

        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2)); //Vest should be offline in this case.

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful - Only from one west
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");

        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 120;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the START was successful
        out.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();

        //Walidate that the vests got stop message.
        socketLine = in.readLine();
        assertEquals("5_STOP", socketLine);

        socketLine = in2.readLine();
        assertEquals("5_STOP", socketLine);

        //Validate that the game is in COMPLETED state
        assertEquals(GameSummary.GameStatusEnum.COMPLETED, game.getStatus());

        //Validate that the response is CONFLICT and contains information.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.CONFLICT.value(), restResponse.getStatus());

        GameStartException exception = new GameStartException().code(GameStartException.CodeEnum.GAME_START_EXCEPTION).detailedInformation("Jedna nebo více vest neodpověděly na START zprávu.");
        assertEquals(jsonMapper.writeValueAsString(exception).trim(), restResponse.getContentAsString().trim());
    }

    @Test
    public void gameStartSecondTryAfterPrepareFail() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests - Login only with both vests.
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);

        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2)); //Vest should be offline in this case.

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful - Only from one west
        out.println("PREPAREDONE");

        //Wait for the request to complete.
        requestThread.join();

        //Validate that the game is back in CREATED state
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());
        assertEquals(GameSummary.GameStatusEnum.CREATED, game.getStatus());

        //Validate that the response is CONFLICT and contains information.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.CONFLICT.value(), restResponse.getStatus());

        GameStartException exception = new GameStartException().code(GameStartException.CodeEnum.GAME_START_EXCEPTION).detailedInformation("Jedna nebo více vest neodpověděly na PREPARE zprávu.");
        assertEquals(jsonMapper.writeValueAsString(exception).trim(), restResponse.getContentAsString().trim());

        //Try second time. Now expect the game was started successfully.
        requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("4_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("4_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the PREPARE was successful
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 120;

        assertEquals("5_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("5_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_5");
        out2.println("ACK_5");

        //Send that the START was successful
        out.println("STARTDONE");
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();


        //Validate the response.
        restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());
    }

    @Test
    public void okGameStartAndFinishTest() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);
        gameData.setDuration(15L);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);
        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2));

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 15;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the START was successful
        out.println("STARTDONE");
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();


        //Validate the response.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());

        //Wait until the game ends and add some buffer to send out the messages.
        long sleepTime = endDate - OffsetDateTime.now().toEpochSecond() + (SOCKET_INTERVAL_BUFFER * 1000L);
        Thread.sleep(sleepTime);

        //Validate that the vests received STOP message after game ended.
        socketLine = in.readLine();
        assertEquals("5_STOP", socketLine);
        socketLine = in2.readLine();
        assertEquals("5_STOP", socketLine);

        //Validate that the game is completed.
        assertEquals(GameSummary.GameStatusEnum.COMPLETED, game.getStatus());
    }

    @Test
    public void okGameStartAndStopCommandTest() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);
        gameData.setDuration(60L);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);
        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2));

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 60;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the START was successful
        out.println("STARTDONE");
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();


        //Validate the response.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());

        mockMvc.perform(get("/api/games/stopGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk());


        //Validate that the vests received STOP message after the stop command was received.
        socketLine = in.readLine();
        assertEquals("5_STOP", socketLine);
        socketLine = in2.readLine();
        assertEquals("5_STOP", socketLine);

        //Validate that the game is completed.
        assertEquals(GameSummary.GameStatusEnum.COMPLETED, game.getStatus());
    }

    @AfterEach
    public void tearDown() {
        playerRepository.deleteAll();
        playerRepository.flush();

        gameRepository.deleteAll();
        gameRepository.flush();

        VestManager.getInstance().deleteAll();
        GamesManager.getInstance().getRunningGames().clear();
    }
}

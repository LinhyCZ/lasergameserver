package cz.zcu.tlinhart.lasergame.server.tests.integration.game;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.game.games.GamesManager;
import cz.zcu.tlinhart.lasergame.server.model.Game;
import cz.zcu.tlinhart.lasergame.server.model.Player;
import cz.zcu.tlinhart.lasergame.server.model.PlayerPosition;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.rest.model.GameSummary;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.StartGameData;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericSocketTest;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GamePositionTest extends GenericSocketTest {

    private String LATITUDE = "45.123455";
    private Float LATITUDE_FLOAT = 45.123455F;
    private String LONGITUDE = "51.898765";
    private Float LONGITUDE_FLOAT = 51.898765F;

    private String LATITUDE2 = "55.874911";
    private Float LATITUDE2_FLOAT = 55.874911F;
    private String LONGITUDE2 = "50.987431";
    private Float LONGITUDE2_FLOAT = 50.987431F;
    @Test
    public void vestSendPositionTest() throws Exception {
        LoginResponse response = login();

        //Register test vests
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_1).vestCode(VEST_CODE_1), response);
        registerVest(new VestRegisterInfo().vestName(VEST_NAME_2).vestCode(VEST_CODE_2), response);

        //Validate that the vests are registered
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_1));
        assertNotNull(VestManager.getInstance().getVestByCode(VEST_CODE_2));

        //Prepare REST API start game data
        StartGameData gameData = generateStartGameData(response);
        gameData.setDuration(60L);

        //Start second vest socket
        prepareSecondClientSocket();

        //Prepare request to StartGame endpoint. Has to be in separate thread.
        AtomicReference<MvcResult> result = new AtomicReference<>();
        Thread requestThread = new Thread(() -> {
            try {
                result.set(mockMvc.perform(post("/api/games/startGame")
                                .header("Authorization", response.getToken())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .content(jsonMapper.writeValueAsString(gameData)))
                        .andDo(print())
                        .andReturn());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //Acknowledge socket welcome
        ackWelcome();
        ackWelcome2();

        //Login vests
        out.println("LOGIN_" + VEST_CODE_1);
        out2.println("LOGIN_" + VEST_CODE_2);

        //Validate that the vests received LOGINOK message.
        String socketLine = in.readLine();
        assertEquals("2_LOGINOK", socketLine);
        socketLine = in2.readLine();
        assertEquals("2_LOGINOK", socketLine);

        //Validate that the vests are online
        Vest vest1 = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Vest vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);

        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest1));
        assertEquals(VestStatus.ONLINE, VestManager.getInstance().getVestStatus(vest2));

        //ACK the LOGINOK message
        out.println("ACK_2");
        out2.println("ACK_2");

        //Vests are ready - Start the request.
        requestThread.start();

        //Validate that the vests received correct prepare informations
        socketLine = in.readLine();
        assertEquals("3_PREP_0_3_0_3_TestPl1_1_1_TestPl2", socketLine);
        socketLine = in2.readLine();
        assertEquals("3_PREP_1_1_0_3_TestPl1_1_1_TestPl2", socketLine);

        //ACK the PREPARE message
        out.println("ACK_3");
        out2.println("ACK_3");

        //Send that the PREPARE was successful
        out.println("PREPAREDONE");
        out2.println("PREPAREDONE");


        //Expect START message. Get start date from Game instance for the test.
        socketLine = in.readLine();
        Game game = GamesManager.getInstance().getRunningGameByID(gameData.getGameID());

        long startDate = game.getStartDate().toEpochSecond();
        long endDate = startDate + 60;

        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);
        socketLine = in2.readLine();
        assertEquals("4_START_30_150_15_30_" + startDate + "_" + endDate, socketLine);

        //ACK the start message.
        out.println("ACK_4");
        out2.println("ACK_4");

        //Send that the START was successful
        out.println("STARTDONE");
        out2.println("STARTDONE");

        //Wait for the request to complete.
        requestThread.join();

        //Validate the response.
        MockHttpServletResponse restResponse = result.get().getResponse();

        assertEquals(HttpStatus.OK.value(), restResponse.getStatus());

        //Let the game to warm up :)
        Thread.sleep(10000);

        out.println("POS_" + LATITUDE + "_" + LONGITUDE);

        //Wait for the command to process
        Thread.sleep(200);

        //Assert that the position is in there
        Vest vest = VestManager.getInstance().getVestByCode(VEST_CODE_1);
        Player player = vest.getPlayer();

        assertEquals(1, player.getPlayerPositions().size());

        PlayerPosition playerPosition = player.getPlayerPositions().get(0);
        assertEquals(LATITUDE_FLOAT, playerPosition.getLatitude());
        assertEquals(LONGITUDE_FLOAT, playerPosition.getLongitude());

        //Assert that the position is not in the second vest
        vest2 = VestManager.getInstance().getVestByCode(VEST_CODE_2);
        Player player2 = vest2.getPlayer();

        assertEquals(new ArrayList<>(), player2.getPlayerPositions());

        //Send and verify second position
        out.println("POS_" + LATITUDE2 + "_" + LONGITUDE2);

        //Wait a bit
        Thread.sleep(200);

        assertEquals(2, player.getPlayerPositions().size());

        playerPosition = player.getPlayerPositions().get(1);
        assertEquals(LATITUDE2_FLOAT, playerPosition.getLatitude());
        assertEquals(LONGITUDE2_FLOAT, playerPosition.getLongitude());


        //Stop the game.
        mockMvc.perform(get("/api/games/stopGame")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk());

        //Validate that the vests received STOP message after the stop command was received.
        socketLine = in.readLine();
        assertEquals("5_STOP", socketLine);
        socketLine = in2.readLine();
        assertEquals("5_STOP", socketLine);

        //Validate that the game is completed.
        assertEquals(GameSummary.GameStatusEnum.COMPLETED, game.getStatus());
    }

    @AfterEach
    public void tearDown() {
        playerPositionRepository.deleteAll();
        playerPositionRepository.flush();

        playerRepository.deleteAll();
        playerRepository.flush();

        gameRepository.deleteAll();
        gameRepository.flush();

        VestManager.getInstance().deleteAll();
        GamesManager.getInstance().getRunningGames().clear();
    }
}

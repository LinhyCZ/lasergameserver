package cz.zcu.tlinhart.lasergame.server.tests.unit;

import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessageIDException;
import cz.zcu.tlinhart.lasergame.server.exceptions.InvalidSocketMessagePayloadException;
import cz.zcu.tlinhart.lasergame.server.socketServer.ISendable;
import cz.zcu.tlinhart.lasergame.server.socketServer.SocketMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

public class SocketMessageTest {

 @Test
 public void intTest() throws Exception {
  int expectedId = 1;
  int expectedPayload = 25;
  ISendable sendable = createISendable(expectedPayload);

  String expectedMessage = "1_25";

  SocketMessage message = new SocketMessage(expectedId, sendable);

  assertEquals(message.toString(), expectedMessage);
 }

 @Test
 public void IntegerTest() throws Exception {
  int expectedId = 1;
  Integer expectedPayload = 30;
  ISendable sendable = createISendable(expectedPayload);

  String expectedMessage = "1_30";

  SocketMessage message = new SocketMessage(expectedId, sendable);

  assertEquals(message.toString(), expectedMessage);
 }

 @Test
 public void StringTest() throws Exception {
  int expectedId = 8;
  String expectedPayload = "testovaci string";
  ISendable sendable = createISendable(expectedPayload);

  String expectedMessage = "8_testovaci string";

  SocketMessage message = new SocketMessage(expectedId, sendable);

  assertEquals(message.toString(), expectedMessage);
 }

 @Test
 public void StringUTF8Test() throws Exception {
  int expectedId = 16;
  String expectedPayload = "testovací string žýářčíěé";
  ISendable sendable = createISendable(expectedPayload);

  String expectedMessage = "16_testovací string žýářčíěé";

  SocketMessage message = new SocketMessage(expectedId, sendable);

  assertEquals(message.toString(), expectedMessage);
 }

 @Test
 public void NegativeIDTest() throws Exception {
  int expectedId = -1;
  String expectedPayload = "test";
  ISendable sendable = createISendable(expectedPayload);

  Exception e = assertThrows(InvalidSocketMessageIDException.class, () -> {
   new SocketMessage(expectedId, sendable);
  });

  assertEquals(e.getMessage(), "Specified ID value of -1 is not a valid message ID.");
 }

 @Test
 public void ZeroIDTest() throws Exception {
  int expectedId = 0;
  String expectedPayload = "test";
  ISendable sendable = createISendable(expectedPayload);

  Exception e = assertThrows(InvalidSocketMessageIDException.class, () -> {
   new SocketMessage(expectedId, sendable);
  });

  assertEquals(e.getMessage(), "Specified ID value of 0 is not a valid message ID.");
 }

 @Test
 public void NullPayloadTest() throws Exception {
  int expectedId = 5;
  ISendable sendable = null;

  Exception e = assertThrows(InvalidSocketMessagePayloadException.class, () -> {
   new SocketMessage(expectedId, sendable);
  });

  assertEquals(e.getMessage(), "Trying to create socket message with null payload.");
 }

 public ISendable createISendable(Object value) {
  return value::toString;
 }
}

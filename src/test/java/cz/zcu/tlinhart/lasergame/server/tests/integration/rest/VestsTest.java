package cz.zcu.tlinhart.lasergame.server.tests.integration.rest;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestCode;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestInfo;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import cz.zcu.tlinhart.lasergame.server.utils.Convert;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VestsTest extends GenericIntegrationTest {

    private static final String VEST_CODE = "J3JUS72BFMMFLAY3";
    private static final String VEST_NAME = "Vesta 1";

    @Test
    public void testControllerWithoutAuthorization() throws Exception {
        assertTrue(testPostEndpointForInvalidAuth("/api/vests/register"));
        assertTrue(testPostEndpointForInvalidAuth("/api/vests/myVests"));
        assertTrue(testDeleteEndpointForInvalidAuth("/api/vests/delete"));
    }

    @Test
    public void testRegisterVest() throws Exception {
        LoginResponse response = login();

        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);

        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());

        Vest vest = vestRepository.findByCode(VEST_CODE);

        // Assert that vest is inserted
        assertNotNull(vest, "Didn't find vest in repository");

        // Assert that the vest is assigned to the user
        User user = userRepository.findByUsername(CORRECT_USERNAME);

        assertEquals(vest.getOwner(), user.getId(), "Vest is assigned to the wrong user");
    }

    @Test
    public void testRegisterVestFailedOnDuplicate() throws Exception {
        LoginResponse response = login();

        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);

        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());

        //Do second register
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isConflict());

        Vest vest = vestRepository.findByCode(VEST_CODE);

        // Assert that vest still exists
        assertNotNull(vest, "Didn't find vest in repository");
        assertEquals(1, vestRepository.findAll().size(), "There are more than one vests connected.");
    }

    @Test
    public void testRegisterVestFailedOnDuplicateWithDifferentUser() throws Exception {
        LoginResponse response = login();

        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);

        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());

        //Logout first user
        logout(response.getToken());

        //Login as second user
        response = login2();

        //Do second register
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isConflict());

        Vest vest = vestRepository.findByCode(VEST_CODE);

        // Assert that vest still exists
        assertNotNull(vest, "Didn't find vest in repository");

        // Assert that the vest is assigned to the first user
        User user = userRepository.findByUsername(CORRECT_USERNAME);

        assertEquals(vest.getOwner(), user.getId(), "Vest is assigned to the wrong user");
    }

    @Test
    public void testMyVestsEmpty() throws Exception {
        LoginResponse response = login();

        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(EMPTY_OBJECT));
    }

    @Test
    public void testMyVestsOneUser() throws Exception {
        LoginResponse response = login();

        // expected
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        VestRegisterInfo registerInfo2 = new VestRegisterInfo().vestCode(VEST_CODE + "2").vestName(VEST_NAME + "2");

        VestInfo info = new VestInfo().vestCode(VEST_CODE).vestName(VEST_NAME).status(VestInfo.StatusEnum.OFFLINE);
        VestInfo info2 = new VestInfo().vestCode(VEST_CODE + "2").vestName(VEST_NAME + "2").status(VestInfo.StatusEnum.OFFLINE);

        List<VestInfo> expectedOneVest = new ArrayList<>();
        expectedOneVest.add(info);

        List<VestInfo> expectedTwoVests = new ArrayList<>();
        expectedTwoVests.add(info);
        expectedTwoVests.add(info2);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));

        // Register second vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo2)))
                .andExpect(status().isOk());

        // Validate that both vests are in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedTwoVests)));
    }

    @Test
    public void testMyVestsTwoUsers() throws Exception {
        LoginResponse response = login();

        // expected
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        VestRegisterInfo registerInfo2 = new VestRegisterInfo().vestCode(VEST_CODE + "2").vestName(VEST_NAME + "2");

        VestInfo info = new VestInfo().vestCode(VEST_CODE).vestName(VEST_NAME).status(VestInfo.StatusEnum.OFFLINE);
        VestInfo info2 = new VestInfo().vestCode(VEST_CODE + "2").vestName(VEST_NAME + "2").status(VestInfo.StatusEnum.OFFLINE);

        List<VestInfo> expectedOneVest = new ArrayList<>();
        expectedOneVest.add(info);

        List<VestInfo> expectedSecondVest = new ArrayList<>();
        expectedSecondVest.add(info2);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));

        // Logout first user and login second
        logout(response.getToken());
        response = login2();

        // Register second vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo2)))
                .andExpect(status().isOk());

        // Validate that only one second vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedSecondVest)));

        // Logout second user and login first
        logout(response.getToken());
        response = login();

        // Validate that the first user still has only first vest assigned to him
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));
    }

    @Test
    public void testVestStatus() throws Exception {
        LoginResponse response = login();

        Vest vest = new Vest();
        vest.setVestCode(VEST_CODE);
        vest.setVestName(VEST_NAME);

        // expected
        VestInfo info = Convert.toVestInfo(vest);
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(info.getVestCode()).vestName(info.getVestName());

        List<VestInfo> expectedVest = new ArrayList<>();
        expectedVest.add(info);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedVest)));

        VestManager.getInstance().connectVest(vest);

        // Expect online status
        info.setStatus(VestInfo.StatusEnum.ONLINE);

        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedVest)));

        VestManager.getInstance().disconnectVest(vest);

        //Expect offline status
        info.setStatus(VestInfo.StatusEnum.OFFLINE);
    }


    @Test
    public void testDeleteVestOneUser() throws Exception {
        LoginResponse response = login();

        // expected
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        VestInfo info = new VestInfo().vestCode(VEST_CODE).vestName(VEST_NAME).status(VestInfo.StatusEnum.OFFLINE);

        List<VestInfo> expectedOneVest = new ArrayList<>();
        expectedOneVest.add(info);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));

        //Delete the first vest
        mockMvc.perform(delete("/api/vests/delete")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(new VestCode().vestCode(VEST_CODE))))
                .andExpect(status().isOk());

        // Validate that the vest is not in the answer anymore
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(EMPTY_OBJECT));
    }

    @Test
    public void testRegisterAfterDeleting() throws Exception {
        LoginResponse response = login();

        // expected
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        VestInfo info = new VestInfo().vestCode(VEST_CODE).vestName(VEST_NAME).status(VestInfo.StatusEnum.OFFLINE);

        List<VestInfo> expectedOneVest = new ArrayList<>();
        expectedOneVest.add(info);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));

        //Delete the first vest
        mockMvc.perform(delete("/api/vests/delete")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(new VestCode().vestCode(VEST_CODE))))
                .andExpect(status().isOk());

        // Validate that the vest is not in the answer anymore
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(EMPTY_OBJECT));

        // Register first vest again
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is again in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));
    }


    @Test
    public void testDeleteVestNonExistingCode() throws Exception {
        LoginResponse response = login();

        // expected
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        VestInfo info = new VestInfo().vestCode(VEST_CODE).vestName(VEST_NAME).status(VestInfo.StatusEnum.OFFLINE);

        List<VestInfo> expectedOneVest = new ArrayList<>();
        expectedOneVest.add(info);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));

        //Try deleting unknown vest
        mockMvc.perform(delete("/api/vests/delete")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(new VestCode().vestCode("UNKNOWN_VEST_CODE"))))
                .andExpect(status().isNoContent());

        // Validate that the first vest is still in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));
    }

    @Test
    public void testDeleteVestTwoUsers() throws Exception {
        LoginResponse response = login();

        // expected
        VestRegisterInfo registerInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        VestRegisterInfo registerInfo2 = new VestRegisterInfo().vestCode(VEST_CODE + "2").vestName(VEST_NAME + "2");

        VestInfo info = new VestInfo().vestCode(VEST_CODE).vestName(VEST_NAME).status(VestInfo.StatusEnum.OFFLINE);
        VestInfo info2 = new VestInfo().vestCode(VEST_CODE + "2").vestName(VEST_NAME + "2").status(VestInfo.StatusEnum.OFFLINE);

        List<VestInfo> expectedOneVest = new ArrayList<>();
        expectedOneVest.add(info);

        List<VestInfo> expectedSecondVest = new ArrayList<>();
        expectedSecondVest.add(info2);

        // Register first vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo)))
                .andExpect(status().isOk());


        // Validate that the vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));

        // Logout first user and login second
        logout(response.getToken());
        response = login2();

        // Register second vest
        mockMvc.perform(post("/api/vests/register")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(registerInfo2)))
                .andExpect(status().isOk());

        // Validate that only one second vest is in the answer
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedSecondVest)));

        //Try deleting first users vest
        mockMvc.perform(delete("/api/vests/delete")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(new VestCode().vestCode(VEST_CODE))))
                .andExpect(status().isConflict());

        // Try deleting second users vest
        mockMvc.perform(delete("/api/vests/delete")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(new VestCode().vestCode(VEST_CODE + "2"))))
                .andExpect(status().isOk());

        // Validate that the vest is actually deleted
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(EMPTY_OBJECT));

        // Logout second user and login first
        logout(response.getToken());
        response = login();

        // Validate that the first user still has his first vest assigned to him
        mockMvc.perform(get("/api/vests/myVests")
                        .header("Authorization", response.getToken())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMapper.writeValueAsString(expectedOneVest)));
    }


    @AfterEach
    public void removeVests() {
        VestManager.getInstance().deleteAll();
    }
}

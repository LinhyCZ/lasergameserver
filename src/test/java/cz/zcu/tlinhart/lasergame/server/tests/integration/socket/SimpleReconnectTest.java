package cz.zcu.tlinhart.lasergame.server.tests.integration.socket;

import cz.zcu.tlinhart.lasergame.server.game.VestManager;
import cz.zcu.tlinhart.lasergame.server.model.Vest;
import cz.zcu.tlinhart.lasergame.server.model.VestStatus;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.rest.model.VestRegisterInfo;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericSocketTest;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class SimpleReconnectTest extends GenericSocketTest {
    String VEST_CODE = "ASDFASDFAF";
    String VEST_NAME = "TestVest1";
    Long VEST_OWNER_MOCK = 123456L;

    @Test
    public void testCorrectVestLogin() throws Exception {

        LoginResponse response = login();

        //Register new vest
        VestRegisterInfo vestInfo = new VestRegisterInfo().vestCode(VEST_CODE).vestName(VEST_NAME);
        registerVest(vestInfo, response);

        //Acknowledge welcome
        ackWelcome();

        //Send login message
        out.println("LOGIN_" + VEST_CODE);

        //Validate socket response
        String socketResponse = in.readLine();
        assertEquals("2_LOGINOK", socketResponse);

        //Validate the vest is online in the manager.
        Vest vest = VestManager.getInstance().getVestByCode(VEST_CODE);
        vest.setOwner(VEST_OWNER_MOCK);
        assertEquals(VestStatus.ONLINE, vest.getStatus());

        out.println("bye");
        out.close();

        Thread.sleep(3000);

        prepareSecondClientSocket();

        ackWelcome2();
        //Send login message
        out2.println("LOGIN_" + VEST_CODE);

        //Validate socket response
        socketResponse = in2.readLine();
        assertEquals("2_LOGINOK", socketResponse);

        //Validate the vest is online in the manager.
        vest = VestManager.getInstance().getVestByCode(VEST_CODE);
        assertEquals(VestStatus.ONLINE, vest.getStatus());
        assertEquals(VEST_OWNER_MOCK, vest.getOwner());

        out2.println("bye");
        out2.close();
    }

    @AfterEach
    public void cleanUp() {
        VestManager.getInstance().disconnectVestByCode(VEST_CODE);
        VestManager.getInstance().deleteAll();
    }

}

package cz.zcu.tlinhart.lasergame.server.tests.integration.rest;

import cz.zcu.tlinhart.lasergame.server.authentication.SessionManager;
import cz.zcu.tlinhart.lasergame.server.model.User;
import cz.zcu.tlinhart.lasergame.server.rest.model.LoginResponse;
import cz.zcu.tlinhart.lasergame.server.tests.integration.utils.GenericIntegrationTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthenticationControllerTest extends GenericIntegrationTest {
    private static final String CORRECT_USERNAME_LOGIN_TEST = "authenticationControllerTest";
    private static final String CORRECT_PASSWORD_LOGIN_TEST = "authenticationControllerTestPassword";
    private static final String INCORRECT_USERNAME_LOGIN_TEST = "incorrect";
    private static final String INCORRECT_PASSWORD_LOGIN_TEST = "incorrectPassword";

    @Autowired
    private SessionManager manager;

    @BeforeAll
    public void setup() {
        manager.registerUser(CORRECT_USERNAME_LOGIN_TEST, CORRECT_PASSWORD_LOGIN_TEST);
    }

    @Test
    public void testLogin() throws Exception {
        String body = "{\"username\": \"" + CORRECT_USERNAME_LOGIN_TEST + "\", \"password\":\"" + CORRECT_PASSWORD_LOGIN_TEST + "\"}";

        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testIncorrectUsername() throws Exception {
        String body = "{\"username\": \"" + INCORRECT_USERNAME_LOGIN_TEST + "\", \"password\":\"" + CORRECT_PASSWORD_LOGIN_TEST + "\"}";

        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }


    @Test
    public void testIncorrectPassword() throws Exception {
        String body = "{\"username\": \"" + CORRECT_USERNAME_LOGIN_TEST + "\", \"password\":\"" + INCORRECT_PASSWORD_LOGIN_TEST + "\"}";

        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void testBothIncorrect() throws Exception {
        String body = "{\"username\": \"" + INCORRECT_USERNAME_LOGIN_TEST + "\", \"password\":\"" + INCORRECT_PASSWORD_LOGIN_TEST + "\"}";

        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void testWrongData() throws Exception {
        String body = "{\"bad_username_key\": \"" + INCORRECT_USERNAME_LOGIN_TEST + "\", \"bad_password_key\":\"" + INCORRECT_PASSWORD_LOGIN_TEST + "\"}";

        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void testLogoff() throws Exception {
        LoginResponse loginResponse = login();

        //Check login result with status endpoint
        mockMvc.perform(statusRequest(loginResponse.getToken()))
                .andExpect(status().isOk());

        //Logout
        mockMvc.perform(get("/logoff")
                        .header("Authorization", loginResponse.getToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //Confirm logging out with request to status endpoint, but receiving 401 Unauthorized status code
        mockMvc.perform(statusRequest(loginResponse.getToken()))
                .andExpect(status().isUnauthorized());

        // Prevent automated logoff on logoff test
        tokenToLogout = null;
    }

    @Test
    public void testLogoverFalse() throws Exception {
        // First login
        LoginResponse loginResponse = login();

        String body = "{\"username\": \"" + CORRECT_USERNAME +"\", \"password\":\"" + CORRECT_PASSWORD + "\", \"logover\": false}";

        // Try logging in with logover set to false
        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().string(""));

        //Confirm first login still works
        mockMvc.perform(statusRequest(loginResponse.getToken()))
                .andExpect(status().isOk());
    }

    @Test
    public void testLogoverNotSet() throws Exception {
        // First login
        LoginResponse loginResponse = login();

        String body = "{\"username\": \"" + CORRECT_USERNAME +"\", \"password\":\"" + CORRECT_PASSWORD + "\"}";

        // Try logging in with logover not set - should default to false
        mockMvc.perform(post("/authenticate")
                        .content(body)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().string(""));

        //Confirm first login still works
        mockMvc.perform(statusRequest(loginResponse.getToken()))
                .andExpect(status().isOk());
    }

    @Test
    public void testLogoverTrue() throws Exception {
        // First login
        LoginResponse loginResponse = login();

        // Second login
        LoginResponse loginResponse2 = login();

        //Confirm first token is logged out
        mockMvc.perform(statusRequest(loginResponse.getToken()))
                .andExpect(status().isUnauthorized());

        //Confirm second token is working fine
        mockMvc.perform(statusRequest(loginResponse2.getToken()))
                .andExpect(status().isOk());
    }

    @AfterAll
    public void tearDown() {
        User user = userRepository.findByUsername(CORRECT_USERNAME_LOGIN_TEST);
        userRepository.deleteById(user.getId());

        userRepository.flush();
    }
}
